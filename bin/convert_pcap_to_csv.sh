#!/usr/bin/env bash

INPUT_FILE_PATH=$1
OUTPUT_DIR_PATH=$2

#required argus-client

which argus
rm /tmp/out.argus || true
argus -r ${INPUT_FILE_PATH} -w /tmp/out.argus
ra -r /tmp/out.argus -s saddr,daddr,proto,sbytes,dbytes,spkts,dpkts -c ',' > ${OUTPUT_DIR_PATH}/flows.csv
rm /tmp/out.argus
