package main;

import main.common.Host;

import java.util.function.Function;

public class Params {
    private static final int DEFAULT_DESTINATION_DIVERSITY_THRESHOLD = 50;
    private static final float DEFAULT_MUTUAL_CONTACT_RATIO_THRESHOLD = 0.03125f;
    private static final float DEFAULT_AVERAGE_DESTINATION_DIVERSITY_RATIO_THRESHOLD = 0.0625f;
    private static final float DEFAULT_AVERAGE_MUTUAL_CONTACT_RATIO_THRESHOLD = 0.25f;

    private static final boolean DEFAULT_USE_LIGHTER_AVGMCR = false;

    private static final Function<Host, Double> DEFAULT_COMMUNITY_NODE_WEIGHT_EXTRACTION_FUNCTION = host -> (double) host.getDestinationDiversityRatio();
    private static final float DEFAULT_NODE_EDGE_WEIGHT_MULTIPLIER = 100f;

    private static final boolean DEFAULT_LOG_COMMUNITIES_DATA = false;
    private static final boolean DEFAULT_LOG_HOSTS_DD_DATA = false;
    private static final boolean DEFAULT_LOG_COMMUNITIES_MCRS = false;

    public int destinationDiversityThreshold;
    public float mutualContactRatioThreshold;
    public float averageDestinationDiversityRatioThreshold;
    public float averageMutualContactRatioThreshold;

    public boolean useLighterAvgMCR;
    public Function<Host, Double> communityNodeWeightExtractionFunction;
    public float nodeEdgeWeightMultiplier;

    public boolean logCommunitiesData;
    public boolean logHostsDDData;
    public boolean logCommunitiesMCRs;


    public Params() {
        this(DEFAULT_DESTINATION_DIVERSITY_THRESHOLD,
                DEFAULT_MUTUAL_CONTACT_RATIO_THRESHOLD,
                DEFAULT_AVERAGE_DESTINATION_DIVERSITY_RATIO_THRESHOLD,
                DEFAULT_AVERAGE_MUTUAL_CONTACT_RATIO_THRESHOLD,
                DEFAULT_USE_LIGHTER_AVGMCR,
                DEFAULT_COMMUNITY_NODE_WEIGHT_EXTRACTION_FUNCTION,
                DEFAULT_NODE_EDGE_WEIGHT_MULTIPLIER,
                DEFAULT_LOG_COMMUNITIES_DATA,
                DEFAULT_LOG_HOSTS_DD_DATA,
                DEFAULT_LOG_COMMUNITIES_MCRS
        );
    }

    public Params(int destinationDiversityThreshold,
                  float mutualContactRatioThreshold,
                  float averageDestinationDiversityRatioThreshold,
                  float averageMutualContactRatioThreshold) {
        this(destinationDiversityThreshold,
                mutualContactRatioThreshold,
                averageDestinationDiversityRatioThreshold,
                averageMutualContactRatioThreshold,
                DEFAULT_USE_LIGHTER_AVGMCR,
                DEFAULT_COMMUNITY_NODE_WEIGHT_EXTRACTION_FUNCTION,
                DEFAULT_NODE_EDGE_WEIGHT_MULTIPLIER,
                DEFAULT_LOG_COMMUNITIES_DATA,
                DEFAULT_LOG_HOSTS_DD_DATA,
                DEFAULT_LOG_COMMUNITIES_MCRS);
    }

    public Params(int destinationDiversityThreshold,
                  float mutualContactRatioThreshold,
                  float averageDestinationDiversityRatioThreshold,
                  float averageMutualContactRatioThreshold,
                  boolean useLighterAvgMCR,
                  Function<Host, Double> communityNodeWeightExtractionFunction,
                  float nodeEdgeWeightMultiplier,
                  boolean logCommunitiesData,
                  boolean logHostsDDData,
                  boolean logCommunitiesMCRs) {
        this.destinationDiversityThreshold = destinationDiversityThreshold;
        this.mutualContactRatioThreshold = mutualContactRatioThreshold;
        this.averageDestinationDiversityRatioThreshold = averageDestinationDiversityRatioThreshold;
        this.averageMutualContactRatioThreshold = averageMutualContactRatioThreshold;
        this.useLighterAvgMCR = useLighterAvgMCR;
        this.communityNodeWeightExtractionFunction = communityNodeWeightExtractionFunction;
        this.nodeEdgeWeightMultiplier = nodeEdgeWeightMultiplier;
        this.logCommunitiesData = logCommunitiesData;
        this.logHostsDDData = logHostsDDData;
        this.logCommunitiesMCRs = logCommunitiesMCRs;
    }

    public void setSimpleNodeWeightExtractionFunction() {
        communityNodeWeightExtractionFunction = (_host) -> 1d;
    }

    public void setStandardNodeWeightExtractionFunction() {
        communityNodeWeightExtractionFunction = (host) -> (double) host.getDestinationDiversityRatio();
    }

    public static Params getDefault() {
        return new Params();
    }
}
