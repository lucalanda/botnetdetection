package main.adapters;

import gnu.trove.map.hash.TIntIntHashMap;
import main.common.Host;
import main.model.MutualContactGraph;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.util.Collections.emptySet;

public class MutualContactGraphAdapter {

    public MutualContactGraph convert(DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph, ArrayList<Host> hosts) {
        TIntIntHashMap hostHashCodeIndexesMap = getHostHashCodeIndexesMap(hosts);

        Set<DefaultWeightedEdge> edges = graph.edgeSet();
        List<String> edgesList = new ArrayList<>();

        for (DefaultWeightedEdge edge : edges) {
            edgesList.add(serializeEdge(graph, hostHashCodeIndexesMap, edge));
        }

        String[] edgesArray = edgesList.toArray(new String[edges.size()]);

        return new MutualContactGraph(edgesArray);
    }

    public DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> revert(MutualContactGraph graph, ArrayList<Host> hosts) {
        return revert(graph, hosts, emptySet());
    }

    public DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> revert(MutualContactGraph graph, ArrayList<Host> hosts, Set<Host> hostsToSkip) {
        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> result = new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);

        hosts.forEach(host -> {
            if (!hostsToSkip.contains(host)) result.addVertex(host);
        });

        for (String serializedEdge : graph.getEdges()) {
            String[] tokens = serializedEdge.split(",");

            Host host1 = hosts.get(parseInt(tokens[0]));
            Host host2 = hosts.get(parseInt(tokens[1]));
            double edgeWeight = parseDouble(tokens[2]);

            if (hostsToSkip.contains(host1) || hostsToSkip.contains(host2)) {
                continue;
            }

            DefaultWeightedEdge edge = result.addEdge(host1, host2);
            result.setEdgeWeight(edge, edgeWeight);
        }

        return result;
    }


    private TIntIntHashMap getHostHashCodeIndexesMap(ArrayList<Host> hosts) {
        TIntIntHashMap result = new TIntIntHashMap(hosts.size(), 0.5F, -1, -1);

        for (int i = 0; i < hosts.size(); i++) {
            result.put(hosts.get(i).hashCode(), i);
        }

        return result;
    }

    private String serializeEdge(Graph<Host, DefaultWeightedEdge> graph, TIntIntHashMap hostHashCodeIndexesMap, DefaultWeightedEdge edge) {
        int host1HashCode = graph.getEdgeSource(edge).hashCode();
        int host2HashCode = graph.getEdgeTarget(edge).hashCode();

        long host1Index = hostHashCodeIndexesMap.get(host1HashCode);
        long host2Index = hostHashCodeIndexesMap.get(host2HashCode);

        if (host1Index == -1 || host2Index == -1)
            throw new IllegalArgumentException("One of these two hosts {" + host1HashCode + "," + host2HashCode + "} was not found in given Host list");

        double weight = graph.getEdgeWeight(edge);

        return host1Index + "," + host2Index + "," + weight;
    }

}
