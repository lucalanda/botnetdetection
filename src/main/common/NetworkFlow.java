package main.common;

import static main.common.IPV4Utils.convertIP;
import static main.common.Utils.matchesIpv4Pattern;

public class NetworkFlow {
    private final int ipSource;
    private final int ipDestination;
    private final byte protocol;
    private final int bytesPerPacketIn;
    private final int bytesPerPacketOut;

    public NetworkFlow(int ipSource, int ipDestination, byte protocol, int bytesPerPacketIn, int bytesPerPacketOut) {
        this.ipSource = ipSource;
        this.ipDestination = ipDestination;
        this.protocol = protocol;
        this.bytesPerPacketIn = bytesPerPacketIn;
        this.bytesPerPacketOut = bytesPerPacketOut;
    }

    public NetworkFlow(String ipSource, String ipDestination, byte protocol, int bytesPerPacketIn, int bytesPerPacketOut) {
//        String ipsFormatException = checkIpsFormat(ipSource, ipDestination);
//        if (ipsFormatException != null) {
//            throw new IllegalArgumentException(ipsFormatException);
//        }

        this.ipSource = convertIP(ipSource);
        this.ipDestination = convertIP(ipDestination);
        this.protocol = protocol;
        this.bytesPerPacketIn = bytesPerPacketIn;
        this.bytesPerPacketOut = bytesPerPacketOut;
    }

    public int getIpSource() {
        return ipSource;
    }

    public int getIpDestination() {
        return ipDestination;
    }

    public byte getProtocol() {
        return protocol;
    }

    public int getBytesPerPacketIn() {
        return bytesPerPacketIn;
    }

    public int getBytesPerPacketOut() {
        return bytesPerPacketOut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NetworkFlow)) return false;

        NetworkFlow that = (NetworkFlow) o;

        if (getIpSource() != that.getIpSource()) return false;
        if (getIpDestination() != that.getIpDestination()) return false;
        if (getProtocol() != that.getProtocol()) return false;
        if (getBytesPerPacketIn() != that.getBytesPerPacketIn()) return false;
        return getBytesPerPacketOut() == that.getBytesPerPacketOut();

    }

    @Override
    public int hashCode() {
        int result = getIpSource();
        result = 31 * result + getIpDestination();
        result = 31 * result + (int) getProtocol();
        result = 31 * result + getBytesPerPacketIn();
        result = 31 * result + getBytesPerPacketOut();
        return result;
    }

    private String checkIpsFormat(String ipSource, String ipDestination) {
        if (!matchesIpv4Pattern(ipSource)) {
            return "ipSource is in wrong format: " + ipSource;
        }
        if (!matchesIpv4Pattern(ipDestination)) {
            return "ipDestination is in wrong format: " + ipDestination;
        }

        return null;
    }
}
