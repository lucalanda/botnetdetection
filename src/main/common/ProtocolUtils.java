package main.common;

import gnu.trove.map.hash.TByteObjectHashMap;
import gnu.trove.map.hash.TObjectByteHashMap;

public class ProtocolUtils {
    private static final byte NO_ENTRY_VALUE = (byte) 1000;

    private static TObjectByteHashMap<String> protocolsConvertMap;
    private static TByteObjectHashMap<String> protocolsRevertMap;

    static {
        protocolsConvertMap = new TObjectByteHashMap<>(3, 0.5F, NO_ENTRY_VALUE);
        protocolsRevertMap = new TByteObjectHashMap<>(3);

        protocolsConvertMap.put("tcp", (byte) 0);
        protocolsConvertMap.put("udp", (byte) 1);
        protocolsConvertMap.put("icmp", (byte) 2);

        protocolsRevertMap.put((byte) 0, "tcp");
        protocolsRevertMap.put((byte) 1, "udp");
        protocolsRevertMap.put((byte) 2, "icmp");
    }

    public static byte convertProtocol(String protocolString) {
        byte protocol = protocolsConvertMap.get(protocolString.toLowerCase());

        if(protocol == NO_ENTRY_VALUE)
            throw new IllegalArgumentException("unsupported protocol: " + protocolString);

        return protocol;
    }

    public static String revertProtocol(byte protocol) {
        String protocolString = protocolsRevertMap.get(protocol);

        if(protocolString == null)
            throw new IllegalArgumentException("unrecognized protocol serialization: " + protocol);

        return protocolString;
    }

    public static boolean isProtocolAccepted(String protocolString) {
        return protocolsConvertMap.get(protocolString.toLowerCase()) != NO_ENTRY_VALUE;
    }

    public static boolean isProtocolAccepted(byte protocol) {
        return protocolsRevertMap.get(protocol) != null;
    }
}
