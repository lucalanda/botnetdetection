package main.common;

import gnu.trove.TIntCollection;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.array.TShortArrayList;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import gnu.trove.set.hash.TShortHashSet;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class IPV4Utils {
    public static int convertIP(String address) {
        int result = 0;

        for(String part : address.split(Pattern.quote("."))) {
            result = result << 8;
            result |= Integer.parseInt(part);
        }

        return result;
    }

    public static String revertIP(int ipv4) {
        return ((ipv4 >> 24 ) & 0xFF) + "." +
                ((ipv4 >> 16 ) & 0xFF) + "." +
                ((ipv4 >>  8 ) & 0xFF) + "." +
                ( ipv4        & 0xFF);
    }

    public static TIntHashSet convertIP(Set<String> ips) {
        TIntHashSet result = new TIntHashSet(ips.size());

        ips.forEach(ip -> result.add(convertIP(ip)));

        return result;
    }

    public static Set<String> revertIP(TIntSet ips) {
        Set<String> result = new HashSet<>(ips.size());

        TIntIterator iterator = ips.iterator();
        while(iterator.hasNext()) result.add(revertIP(iterator.next()));

        return result;
    }

    public static short convertPrefix(String ip16Prefix) {
        return (short) convertIP(ip16Prefix);
    }

    public static TShortHashSet convertPrefixes(Set<String> ip16Prefixes) {
        TShortHashSet result = new TShortHashSet();

        ip16Prefixes.forEach(ip -> result.add(convertPrefix(ip)));
        return result;
    }

    public static short ip16Prefix(int ipv4) {
        return (short) (ipv4 >> 16);
    }

    public static TIntHashSet convertedIpv4Set(String... ips) {
        TIntHashSet result = new TIntHashSet();

        for(String ip : ips) result.add(convertIP(ip));

        return result;
    }

    public static TShortHashSet ip16Prefixes(TIntCollection ipv4s) {
        TShortHashSet result = new TShortHashSet();

        TIntIterator iterator = ipv4s.iterator();
        while (iterator.hasNext()) {
            result.add(ip16Prefix(iterator.next()));
        }

        return result;
    }

    public static TShortArrayList ip16Prefixes(Collection<String> ips) {
        TShortArrayList result = new TShortArrayList(ips.size());

        ips.forEach(ip -> result.add(ip16Prefix(convertIP(ip))));

        return result;
    }
}
