package main.common;

import java.util.Objects;

public class Couple<T> {
    public final T first;
    public final T second;

    public Couple(T first, T second) {
        if(first.hashCode() < second.hashCode()) {
            this.first = first;
            this.second = second;
        } else {
            this.first = second;
            this.second = first;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

    @Override
    public boolean equals(Object obj) {
        return this.hashCode() == obj.hashCode();
    }
}
