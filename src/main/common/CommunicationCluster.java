package main.common;

import gnu.trove.set.hash.TIntHashSet;
import gnu.trove.set.hash.TShortHashSet;

import java.io.Serializable;

import static main.common.IPV4Utils.ip16Prefix;

public class CommunicationCluster implements Serializable {
    static final long serialVersionUID = 42L;

    private final int[] contacts;
    private final byte protocol;
    private final int bytesPerPacketIn;
    private final int bytesPerPacketOut;
    private final int preComputedHashCode;
    private boolean isP2P;

    public CommunicationCluster(int[] contacts, byte protocol, int bytesPerPacketIn, int bytesPerPacketOut, boolean isP2P) {
        this.contacts = contacts;
        this.protocol = protocol;
        this.bytesPerPacketIn = bytesPerPacketIn;
        this.bytesPerPacketOut = bytesPerPacketOut;
        this.isP2P = isP2P;
        this.preComputedHashCode = computeHashCode();
    }

    public CommunicationCluster(TIntHashSet contacts, byte protocol, int bytesPerPacketIn, int bytesPerPacketOut) {
        this(contacts.toArray(), protocol, bytesPerPacketIn, bytesPerPacketOut);
    }

    public CommunicationCluster(int[] contacts, byte protocol, int bytesPerPacketIn, int bytesPerPacketOut) {
        this(contacts, protocol, bytesPerPacketIn, bytesPerPacketOut, false);
    }

    public static CommunicationCluster newEmptyFrom(NetworkFlow nf) {
        return new CommunicationCluster(null, nf.getProtocol(), nf.getBytesPerPacketIn(), nf.getBytesPerPacketOut(), false);
    }

    public CommunicationCluster updatedInstance(int[] newContacts) {
        TIntHashSet updatedContactSet = new TIntHashSet(contacts);
        updatedContactSet.addAll(newContacts);

        return new CommunicationCluster(updatedContactSet.toArray(), protocol, bytesPerPacketIn, bytesPerPacketOut);
    }

    @Override
    public boolean equals(Object o) {
        return this.hashCode() == o.hashCode();
    }

    @Override
    public int hashCode() {
        return preComputedHashCode;
    }

    @Override
    public String toString() {
        return "CommunicationCluster{" +
                "contacts=" + getContacts() +
                ", protocol=" + getProtocol() +
                ", bytesPerPacketIn=" + getBytesPerPacketIn() +
                ", bytesPerPacketOut=" + getBytesPerPacketOut() +
                ", isP2P=" + isP2P() +
                '}';
    }

    public int[] getContacts() { return contacts; }

    public byte getProtocol() {
        return protocol;
    }

    public int getBytesPerPacketIn() {
        return bytesPerPacketIn;
    }

    public int getBytesPerPacketOut() {
        return bytesPerPacketOut;
    }

    public void setP2P(boolean p2P) {
        isP2P = p2P;
    }

    public boolean isP2P() {
        return isP2P;
    }

    public short[] getIp16Prefixes() { return extractIP16Prefixes(contacts); }

    private int computeHashCode() {
        return computeHashCodeFor(getProtocol(), getBytesPerPacketIn(), getBytesPerPacketOut());
    }

    public static int computeHashCodeFor(byte protocol, int bytesPerPacketIn, int bytesPerPacketOut) {
        int result = (int) protocol;
        result = 31 * result + bytesPerPacketIn;
        result = 31 * result + bytesPerPacketOut;
        return result;
    }

    private static short[] extractIP16Prefixes(int[] contacts) {
        TShortHashSet result = new TShortHashSet();

        for(int contact : contacts) {
            result.add(ip16Prefix(contact));
        }

        return result.toArray();
    }
}
