package main.common;

import gnu.trove.iterator.hash.TObjectHashIterator;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.set.hash.THashSet;
import gnu.trove.set.hash.TIntHashSet;

import java.io.Serializable;
import java.util.Set;

import static main.common.Utils.destinationDiversityRatio;

public class Host implements Serializable {
    static final long serialVersionUID = 42L;

    private boolean isP2P;
    private final int ip;
    private CommunicationCluster[] communicationClusters;
    private int preComputedHashCode;

    public Host(boolean isP2P, int ip, Set<CommunicationCluster> communicationClusters) {
        this(isP2P, ip, communicationClusters.toArray(new CommunicationCluster[communicationClusters.size()]));
    }

    public Host(boolean isP2P, int ip, CommunicationCluster[] communicationClusters) {
        this.isP2P = isP2P;
        this.ip = ip;
        this.communicationClusters = communicationClusters;
        this.preComputedHashCode = computeHashCode();
    }

    public void update(Set<CommunicationCluster> newCommunicationClusters) {
        THashSet<CommunicationCluster> currentCommunicationClusters = new THashSet<>(communicationClusters.length);
        THashSet<CommunicationCluster> newCommuncationClusters = new THashSet<>();

        for (CommunicationCluster c : communicationClusters) {
            currentCommunicationClusters.add(c);
        }

        TObjectIntHashMap<CommunicationCluster> clusterIndexMap = new TObjectIntHashMap<>();
        for (int i = 0; i < communicationClusters.length; i++) {
            clusterIndexMap.put(communicationClusters[i], i);
        }

        for (CommunicationCluster newCluster : newCommunicationClusters) {
            if (currentCommunicationClusters.contains(newCluster)) {
                int index = clusterIndexMap.get(newCluster);
                CommunicationCluster currentCluster = communicationClusters[index];

                communicationClusters[index] = currentCluster.updatedInstance(newCluster.getContacts());
            } else {
                newCommuncationClusters.add(newCluster);
            }
        }

        CommunicationCluster[] newCommunicationClusterSet = new CommunicationCluster[communicationClusters.length + newCommuncationClusters.size()];

        int i = 0;
        for(CommunicationCluster c : communicationClusters) newCommunicationClusterSet[i++] = c;

        TObjectHashIterator<CommunicationCluster> newCommunicationClustersIterator = newCommuncationClusters.iterator();
        while(newCommunicationClustersIterator.hasNext()) {
            newCommunicationClusterSet[i++] = newCommunicationClustersIterator.next();
        }

        communicationClusters = newCommunicationClusterSet;
        preComputedHashCode = computeHashCode();
    }

    @Override
    public Host clone() {
        return new Host(isP2P, ip, communicationClusters);
    }

    @Override
    public String toString() {
        return "Host{" +
                "isP2P=" + isP2P +
                ", ip='" + ip + '\'' +
                ", communicationClusters=" + communicationClusters +
                ", preComputedHashCode=" + preComputedHashCode +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Host)) return false;
        return this.realHashCode() == ((Host) o).realHashCode();
    }

    @Override
    public int hashCode() {
        return ip;
    }

    public int realHashCode() {
        return preComputedHashCode;
    }

    public void setP2P(boolean isP2P) {
        this.isP2P = isP2P;
    }

    public boolean isP2P() {
        return isP2P;
    }

    public int getIp() {
        return ip;
    }

    public float getDestinationDiversityRatio() {
        return destinationDiversityRatio(getP2PContacts());
    }

    public CommunicationCluster[] getCommunicationClusters() {
        return communicationClusters;
    }

    public Set<CommunicationCluster> getP2PCommunicationClusters() {
        Set<CommunicationCluster> result = new THashSet<>();

        for (CommunicationCluster c : communicationClusters) {
            if (c.isP2P()) result.add(c);
        }

        return result;
    }

    public TIntHashSet getP2PContacts() {
        TIntHashSet result = new TIntHashSet();

        for (CommunicationCluster c : communicationClusters) {
            if (c.isP2P()) result.addAll(c.getContacts());
        }

        return result;
    }

    private int getContactsSize() {
        int result = 0;

        for (CommunicationCluster c : communicationClusters) {
            result += c.getContacts().length;
        }

        return result;
    }

    private int computeHashCode() {
        int result = getIp();
        result = 31 * result + getContactsSize();
        result = 31 * result + getCommunicationClusters().length;
        return result;
    }
}
