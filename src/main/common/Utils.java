package main.common;

import gnu.trove.TIntCollection;
import gnu.trove.set.hash.TIntHashSet;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import static main.common.IPV4Utils.ip16Prefixes;

public class Utils {
    private final static String IPV4_REGEX = "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$";
    //    private final static String IPV6_REGEX = "(?![^\\w:])(([0-9a-f]{1,4}:){1,7}:|:(:[0-9a-f]{1,4}){1,7}|([0-9a-f]{1,4}:){1,7}[0-9a-f]{0,4}(:[0-9a-f]{1,4}){1,7})(?![\\w:])";
    private static final Pattern IPV4_PATTERN = Pattern.compile(IPV4_REGEX);
//    private static final Pattern IPV6_PATTERN = Pattern.compile(IPV6_REGEX);

    public static boolean matchesIpv4Pattern(String ip) {
        return IPV4_PATTERN.matcher(ip).matches();
    }

    public static String ip16Prefix(String ip) {
        if (matchesIpv4Pattern(ip)) {
            String[] tokens = ip.split("\\.");
            return tokens[0] + "." + tokens[1];
        } else {
            throw new IllegalArgumentException("cannot find 16bit prefix for incorrect ip: " + ip);
        }
    }

    public static int destinationDiversity(TIntCollection ipDestinations) {
        return ip16Prefixes(ipDestinations).size();
    }

    public static float destinationDiversityRatio(TIntCollection ipList) {
        float ddr = (float) destinationDiversity(ipList) / (float) ipList.size();
        float roundedDdr = ((int) (ddr * 1000)) / 1000f;
        return roundedDdr;
    }

    public static float mutualContactRatio(TIntHashSet set1, TIntHashSet set2) {
        TIntHashSet union = new TIntHashSet(set1);
        union.addAll(set2);

        TIntHashSet intersection = new TIntHashSet(set1);
        intersection.retainAll(set2);

        int unionSize = union.size();
        int intersectionSize = intersection.size();

        return (float) intersectionSize / (float) unionSize;
    }

    public static <T> boolean setsHaveIntersection(Set<T> set1, Set<T> set2) {
        for (T t : set1) {
            if (set2.contains(t)) {
                return true;
            }
        }

        return false;
    }

    public static <T> Set<Couple<T>> getCouples(Collection<T> set) {
        Set<Couple<T>> result = new HashSet<>();

        for (T i : set) {
            for (T j : set) {
                if (i.equals(j)) continue;
                result.add(new Couple<>(i, j));
            }
        }

        return result;
    }
}
