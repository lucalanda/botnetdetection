package main;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.hash.THashMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.hash.THashSet;
import gnu.trove.set.hash.TIntHashSet;
import main.common.CommunicationCluster;
import main.common.Host;
import main.common.NetworkFlow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import static test.util.Util.buildArray;

public class HostsExtractor {

    private final ArrayList<Host> currentHosts;

    public HostsExtractor(ArrayList<Host> currentHosts) {
        this.currentHosts = currentHosts;
    }

    public HostsExtractor() {
        this.currentHosts = new ArrayList<>();
    }

    public ArrayList<Host> extractUpdatedHostsList(Collection<NetworkFlow> networkFlows) {
        TIntIntHashMap currentHostIpIndexesMap = currentHostIpIndexesMap();

        TIntHashSet hostIps = new TIntHashSet();
        TIntObjectHashMap<THashMap<CommunicationCluster, TIntHashSet>> ipCommunicationClusterContactSetMap = new TIntObjectHashMap<>();

        networkFlows.forEach(networkFlow -> {
            int hostIp = networkFlow.getIpSource();

            if (!hostIps.contains(hostIp)) {
                hostIps.add(hostIp);
                ipCommunicationClusterContactSetMap.put(hostIp, new THashMap<>());
            }

            Map<CommunicationCluster, TIntHashSet> communicationClustersContactSet = ipCommunicationClusterContactSetMap.get(hostIp);
            CommunicationCluster cluster = CommunicationCluster.newEmptyFrom(networkFlow);
            if (!communicationClustersContactSet.containsKey(cluster)) {
                communicationClustersContactSet.put(cluster, new TIntHashSet());
            }

            communicationClustersContactSet.get(cluster).add(networkFlow.getIpDestination());
        });

        ArrayList<Host> result = new ArrayList<>(hostIps.size());
        result.addAll(currentHosts);

        TIntIterator hostIpsIterator = hostIps.iterator();
        while (hostIpsIterator.hasNext()) {
            int ip = hostIpsIterator.next();
            Map<CommunicationCluster, TIntHashSet> communicationClustersContactSet = ipCommunicationClusterContactSetMap.get(ip);

            THashSet<CommunicationCluster> clusters = new THashSet<>(communicationClustersContactSet.size());

            communicationClustersContactSet.forEach((cluster, contactSet) -> {
                CommunicationCluster newCluster = new CommunicationCluster(
                        contactSet.toArray(),
                        cluster.getProtocol(),
                        cluster.getBytesPerPacketIn(),
                        cluster.getBytesPerPacketOut()
                );
                clusters.add(newCluster);
            });

            if (currentHostIpIndexesMap.containsKey(ip)) {
                int index = currentHostIpIndexesMap.get(ip);
                result.get(index).update(clusters);
            } else {
                result.add(new Host(false, ip, buildArray(clusters)));
            }
        }

        return result;
    }

    private TIntIntHashMap currentHostIpIndexesMap() {
        TIntIntHashMap result = new TIntIntHashMap((int) (currentHosts.size() * 1.2));

        for (int i = 0; i < currentHosts.size(); i++) {
            result.put(currentHosts.get(i).getIp(), i);
        }

        return result;
    }

}
