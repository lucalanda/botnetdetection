package main.model;

import java.io.Serializable;
import java.util.Arrays;

public class MutualContactGraph implements Serializable {
    static final long serialVersionUID = 42L;

    private final String[] edges;

    public MutualContactGraph(String[] edges) {
        this.edges = edges;
    }

    public static MutualContactGraph getEmptyGraph() {
        return new MutualContactGraph(new String[0]);
    }

    @Override
    public boolean equals(Object obj) {
        return this.hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getEdges());
    }

    public String[] getEdges() {
        return edges;
    }

}
