package main.model;

import main.common.Host;

import java.io.*;
import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;

public class DetectionData implements Serializable {
    static final long serialVersionUID = 42L;

    private final List<Host> hosts;
    private final MutualContactGraph graph;
    private final Set<String> botIps;

    public DetectionData(List<Host> hosts, MutualContactGraph graph, Set<String> botIps) {
        this.hosts = hosts;
        this.graph = graph;
        this.botIps = botIps;
    }

    public static DetectionData getEmptyData() {
        return new DetectionData(emptyList(), MutualContactGraph.getEmptyGraph(), emptySet());
    }

    public static byte[] serializeData(DetectionData detectionData) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(detectionData);
        return out.toByteArray();
    }

    public static DetectionData deserializeData(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return (DetectionData) is.readObject();
    }

    public MutualContactGraph getGraph() {
        return graph;
    }

    public Set<String> getBotIps() {
        return botIps;
    }

    public List<Host> getHosts() { return hosts; }
}
