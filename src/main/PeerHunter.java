package main;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.set.hash.THashSet;
import main.adapters.MutualContactGraphAdapter;
import main.common.CommunicationCluster;
import main.common.Host;
import main.common.IPV4Utils;
import main.common.NetworkFlow;
import main.detection.MutualContactGraphExtractor;
import main.detection.P2PBotsDetector;
import main.detection.P2PHostsDetector;
import main.model.DetectionData;
import main.model.MutualContactGraph;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static main.common.IPV4Utils.revertIP;

public class PeerHunter {
    private final Params params;
    private final ArrayList<Host> currentHosts;
    private final MutualContactGraph currentSerializedMutualContactGraph;
    private final MutualContactGraphAdapter mutualContactGraphAdapter;

    public PeerHunter(Params params, DetectionData detectionData) {
        this.params = params;
        this.currentHosts = new ArrayList<>(detectionData.getHosts());
        this.currentSerializedMutualContactGraph = detectionData.getGraph();
        this.mutualContactGraphAdapter = new MutualContactGraphAdapter();
    }

    public PeerHunter(DetectionData detectionData) {
        this(Params.getDefault(), detectionData);
    }

    public PeerHunter(Params params) {
        this(params, DetectionData.getEmptyData());
    }

    public PeerHunter() {
        this(Params.getDefault());
    }

    // old optimized version: it extracts updated hosts list correctly, but there is some problem after reverting the MutualContactGraph from DetectionData

//    public DetectionData computeDetectionData(Collection<NetworkFlow> networkFlows) {
//        TIntArrayList currentHostHashCodes = getHostHashCodes(currentHosts);
//
//        ArrayList<Host> hosts = new HostsExtractor(currentHosts).extractUpdatedHostsList(networkFlows);
//
//        List<Host> newOrUpdatedHosts = getNewOrUpdatedHosts(currentHostHashCodes, hosts);
//
//        new P2PHostsDetector(params).flagP2PHosts(newOrUpdatedHosts);
//
//        THashSet<Host> newOrUpdatedP2PHosts = extractP2PHosts(newOrUpdatedHosts);
//
//        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> currentMutualContactGraph =
//                mutualContactGraphAdapter.revert(currentSerializedMutualContactGraph, currentHosts, newOrUpdatedP2PHosts);
//        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> newMutualContactGraph =
//                new MutualContactGraphExtractor(params, currentMutualContactGraph).getGraph(newOrUpdatedP2PHosts);
//
//        Set<Host> bots = new P2PBotsDetector(newMutualContactGraph, params).getBots();
//
//        MutualContactGraph mutualContactGraph = mutualContactGraphAdapter.convert(newMutualContactGraph, hosts);
//        Set<String> botIpsSet = bots.stream().map(Host::getIp).map(IPV4Utils::revertIP).collect(toSet());
//        return new DetectionData(hosts, mutualContactGraph, botIpsSet);
//    }

    public DetectionData computeDetectionData(Collection<NetworkFlow> networkFlows) {
        ArrayList<Host> hosts = new HostsExtractor(currentHosts).extractUpdatedHostsList(networkFlows);

        new P2PHostsDetector(params).flagP2PHosts(hosts);

        THashSet<Host> p2pHosts = extractP2PHosts(hosts);

        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> newMutualContactGraph =
                new MutualContactGraphExtractor(params).getGraph(p2pHosts);

        Set<Host> bots = new P2PBotsDetector(newMutualContactGraph, params).getBots();

        MutualContactGraph mutualContactGraph = mutualContactGraphAdapter.convert(newMutualContactGraph, hosts);
        Set<String> botIpsSet = bots.stream().map(Host::getIp).map(IPV4Utils::revertIP).collect(toSet());

        DetectionData result = new DetectionData(hosts, mutualContactGraph, botIpsSet);

        if(params.logHostsDDData) {
            logHostsDDData(result);
        }

        return result;
    }

    public List<Host> getNewOrUpdatedHosts(TIntArrayList oldHostHashCodes, ArrayList<Host> newHostList) {
        List<Host> result = new ArrayList<>();

        for (int i = 0; i < oldHostHashCodes.size(); i++) {
            if (oldHostHashCodes.get(i) != newHostList.get(i).realHashCode()) {
                result.add(newHostList.get(i));
            }
        }

        result.addAll(newHostList.subList(oldHostHashCodes.size(), newHostList.size()));

        return result;
    }

    private THashSet<Host> extractP2PHosts(List<Host> newOrUpdatedHosts) {
        THashSet<Host> result = new THashSet<>();

        for(Host h: newOrUpdatedHosts) {
            if(h.isP2P()) result.add(h);
        }

        return result;
    }

    private TIntArrayList getHostHashCodes(ArrayList<Host> hosts) {
        TIntArrayList result = new TIntArrayList(hosts.size());

        for(Host h : hosts) result.add(h.realHashCode());

        return result;
    }

    private void logHostsDDData(DetectionData detectionData) {
        System.out.println("bot ips invididual dd and ddr values:");
        for (Host h : detectionData.getHosts()) {
            String revertedIP = revertIP(h.getIp());
            if (detectionData.getBotIps().contains(revertedIP)) {
                System.out.println(revertedIP + " - top dd:" + topDestinationDiversityForHost(h) + ", ddr: " + h.getDestinationDiversityRatio());
            }
        }
    }

    private static int topDestinationDiversityForHost(Host h) {
        int result = 0;

        for (CommunicationCluster c : h.getCommunicationClusters()) {
            result = Math.max(result, c.getIp16Prefixes().length);
        }

        return result;
    }
}
