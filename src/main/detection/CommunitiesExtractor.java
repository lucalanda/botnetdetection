package main.detection;

import cwts.networkanalysis.Clustering;
import cwts.networkanalysis.IterativeCPMClusteringAlgorithm;
import cwts.networkanalysis.LouvainAlgorithm;
import cwts.networkanalysis.Network;
import gnu.trove.map.hash.TObjectIntHashMap;
import main.Params;
import main.common.Host;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.stream.IntStream.range;

public class CommunitiesExtractor {
    private final DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph;
    private final Params params;

    public CommunitiesExtractor(DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph, Params params) {
        this.graph = graph;
        this.params = params;
    }

    public CommunitiesExtractor(DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph) {
        this(graph, Params.getDefault());
    }

    public Collection<Set<Host>> getCommunities() {
        Host[] hostsArray = toArray(graph.vertexSet());
        TObjectIntHashMap<Host> hostIdsMap = getHostsIdsMap(hostsArray);

        Network network = buildNetwork(hostIdsMap);
        Clustering clustering = new Clustering(network.getNNodes());

        if(clustering.getNNodes() > 0) {
            IterativeCPMClusteringAlgorithm algorithm = new LouvainAlgorithm();
            algorithm.improveClustering(network, clustering);
            return convertClusteringToCommunitiesCollection(clustering, hostsArray);
        } else {
            return emptyList();
        }
    }

    public Collection<Set<Host>> convertClusteringToCommunitiesCollection(Clustering clustering, Host[] hosts) {
        int nClusters = clustering.getNClusters();
        int[] clustersMemberships = clustering.getClusters();

        ArrayList<Set<Host>> result = new ArrayList<>(clustering.getNClusters());
        range(0, nClusters).forEach(_i -> result.add(new HashSet<>()));

        range(0, clustersMemberships.length).forEach(i -> {
            int cluster = clustersMemberships[i];
            result.get(cluster).add(hosts[i]);
        });

        return result;
    }

    public Network buildNetwork(TObjectIntHashMap<Host> hostIdsMap) {
        int nodesNumber = graph.vertexSet().size();
        int edgesNumber = graph.edgeSet().size();

        double[] nodeWeights = new double[nodesNumber];
        int[][] edges = new int[2][edgesNumber];
        double[] edgeWeights = new double[edgesNumber];

        hostIdsMap.forEachEntry((host, index) -> {
            nodeWeights[index] = params.communityNodeWeightExtractionFunction.apply(host);
            return true;
        });

        int edgesCounter = 0;
        for(DefaultWeightedEdge edge : graph.edgeSet()) {
            Host host1 = graph.getEdgeSource(edge);
            Host host2 = graph.getEdgeTarget(edge);
            int host1Id = hostIdsMap.get(host1);
            int host2Id = hostIdsMap.get(host2);
            double edgeWeight = graph.getEdgeWeight(edge);

            edges[0][edgesCounter] = host1Id;
            edges[1][edgesCounter] = host2Id;
            edgeWeights[edgesCounter] = getNetworkEdgeWeight(edgeWeight);

            edgesCounter++;
        }

        return new Network(nodeWeights, edges, edgeWeights, false, false);
    }

    private double getNetworkEdgeWeight(double edgeWeight) {
        return edgeWeight * params.nodeEdgeWeightMultiplier;
    }

    public TObjectIntHashMap<Host> getHostsIdsMap(Host[] hosts) {
        TObjectIntHashMap<Host> result = new TObjectIntHashMap<>(hosts.length);

        for(int i = 0; i < hosts.length; i++) {
            result.put(hosts[i], i);
        }

        return result;
    }

    public Host[] toArray(Collection<Host> hosts) {
        Host[] result = new Host[hosts.size()];
        return hosts.toArray(result);
    }
}
