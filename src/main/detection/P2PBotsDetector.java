package main.detection;

import main.Params;
import main.common.Couple;
import main.common.Host;
import main.common.Utils;
import org.jgrapht.alg.clique.BronKerboschCliqueFinder;
import org.jgrapht.graph.AsSubgraph;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.toList;
import static main.common.IPV4Utils.revertIP;
import static main.common.Utils.mutualContactRatio;

public class P2PBotsDetector {

    private final DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> hostsGraph;
    private final Params params;

    public P2PBotsDetector(DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> hostsGraph, Params params) {
        this.hostsGraph = hostsGraph;
        this.params = params;
    }

    public P2PBotsDetector(DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> hostsGraph) {
        this(hostsGraph, Params.getDefault());
    }

    public Set<Host> getBots() {
        Collection<Set<Host>> communities = getCommunities();
        if (params.logCommunitiesMCRs) {
            logCommunitiesMCRs(communities);
        }

        Collection<Set<Host>> botnets = communities.stream().filter(this::isBotnet).collect(toList());

        Set<Host> result = new HashSet<>();
        botnets.forEach(botnet ->
                result.addAll(extractBots(hostsGraph, botnet)));

        return result;
    }

    public Set<Host> extractBots(DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> hostsGraph, Set<Host> botnet) {
        Set<Host> result = new HashSet<>();

        AsSubgraph<Host, DefaultWeightedEdge> subgraph = new AsSubgraph<>(hostsGraph, botnet);

        boolean found;
        do {
            found = false;

            Iterator cliquesIterator = new BronKerboschCliqueFinder(subgraph).maximumIterator();

            while (cliquesIterator.hasNext()) {
                Set<Host> clique = (Set<Host>) cliquesIterator.next();
                if (clique.size() >= 3) {
                    found = true;
                    result.addAll(clique);
                    subgraph.removeAllVertices(clique);
                }
            }
        } while (found);

        return result;
    }

    public boolean isBotnet(Set<Host> community) {
        if(community.size() == 1) return false;

        double avgddr = getAverageDestinationDiversityRatio(community);
        float avgmcr = getAverageMutualContactsRatio(community);

        boolean res = avgddr >= params.averageDestinationDiversityRatioThreshold &&
                avgmcr >= params.averageMutualContactRatioThreshold;

        if(params.logCommunitiesData) {
            System.out.println(community.stream().map(host -> revertIP(host.getIp())).collect(toList()));
            System.out.println("avgddr: " + avgddr + " threshold is: " + params.averageDestinationDiversityRatioThreshold);
            System.out.println("avgmcr: " + avgmcr + " threshold is: " + params.averageMutualContactRatioThreshold);
            System.out.println(res);
        }
        return res;
    }

    private Collection<Set<Host>> getCommunities() {
        CommunitiesExtractor detector = new CommunitiesExtractor(hostsGraph);
        return detector.getCommunities();
    }

    public double getAverageDestinationDiversityRatio(Set<Host> community) {
        return community.stream()
                .map(Host::getDestinationDiversityRatio)
                .reduce(Float::sum).get() / (float) community.size();
    }

    public float getAverageMutualContactsRatio(Set<Host> community) {
        if(params.useLighterAvgMCR) return getAverageMutualContactsRatioLighter(community);
        return getAverageMutualContactsRatioStandard(community);
    }

    private float getAverageMutualContactsRatioLighter(Set<Host> community) {
        Set<Couple<Host>> couples = Utils.getCouples(community);

        if (couples.size() > 0) {
            return couples.stream()
                    .map(couple -> mutualContactRatio(couple.first.getP2PContacts(), couple.second.getP2PContacts()))
                    .reduce(Float::sum).get() / (float) community.size();
        } else {
            return 0;
        }
    }

    private float getAverageMutualContactsRatioStandard(Set<Host> community) {
        Set<Couple<Host>> couples = Utils.getCouples(community);

        if (couples.size() > 0) {
            float result = 2 * couples.stream()
                    .map(couple -> mutualContactRatio(couple.first.getP2PContacts(), couple.second.getP2PContacts()))
                    .reduce(Float::sum).get();

            result /= (community.size() * (community.size() - 1));
            return result;
        } else {
            return 0;
        }
    }

    private void logCommunitiesMCRs(Collection<Set<Host>> communities) {
        AtomicInteger counter = new AtomicInteger();

        System.out.println("##### Start communities MCRs logging #####");

        communities.forEach(hostSet -> {
            System.out.println("****************************************");
            System.out.println("Community " + counter.getAndIncrement());
            System.out.println();
            System.out.println("AVG MCR: " + getAverageMutualContactsRatio(hostSet) + ", threshold: " + params.averageMutualContactRatioThreshold);
            System.out.println();
            Utils.getCouples(hostSet).forEach(couple -> {
                float mcr = Utils.mutualContactRatio(couple.first.getP2PContacts(), couple.second.getP2PContacts());

                String logLine = "(" + revertIP(couple.first.getIp()) + ", " + revertIP(couple.second.getIp()) + ") - " + mcr;
                System.out.println(logLine);
            });
            System.out.println();
            System.out.println("****************************************");
        });

        System.out.println("##### End communities MCRs logging #####");
    }
}
