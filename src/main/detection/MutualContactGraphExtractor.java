package main.detection;

import main.Params;
import main.common.Couple;
import main.common.Host;
import main.common.Utils;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.Collection;

import static main.common.Utils.mutualContactRatio;
import static main.common.Utils.setsHaveIntersection;

public class MutualContactGraphExtractor {

    private final float mutualContactRatioThreshold;
    private final DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> currentMutualContactGraph;

    public MutualContactGraphExtractor(float mutualContactRatioThreshold,
                                       DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> currentMutualContactGraph) {
        this.mutualContactRatioThreshold = mutualContactRatioThreshold;
        this.currentMutualContactGraph = currentMutualContactGraph;
    }

    public MutualContactGraphExtractor(Params params,
                                       DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> currentMutualContactGraph) {
        this(params.mutualContactRatioThreshold, currentMutualContactGraph);
    }

    public MutualContactGraphExtractor(Params params) {
        this(params.mutualContactRatioThreshold, getEmptyGraph());
    }

    public MutualContactGraphExtractor(float mutualContactRatioThreshold) {
        this(mutualContactRatioThreshold, getEmptyGraph());
    }

    public DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> getGraph(Collection<Host> newHosts) {
        final DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> result = currentMutualContactGraph;

        newHosts.forEach(result::addVertex);

        for (Couple<Host> couple : Utils.getCouples(newHosts)) {
            Host host1 = couple.first, host2 = couple.second;

            if (!setsHaveIntersection(host1.getP2PCommunicationClusters(), host2.getP2PCommunicationClusters())) {
                continue;
            }

            float mutualContactRatio = mutualContactRatio(host1.getP2PContacts(), host2.getP2PContacts());

            if (mutualContactRatio < mutualContactRatioThreshold) {
                continue;
            }

            DefaultWeightedEdge edge = result.addEdge(host1, host2);
            result.setEdgeWeight(edge, mutualContactRatio);
        }

        return result;
    }

    private static DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> getEmptyGraph() {
        return new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);
    }

}
