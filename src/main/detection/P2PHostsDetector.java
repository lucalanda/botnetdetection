package main.detection;

import main.Params;
import main.common.CommunicationCluster;
import main.common.Host;

import java.util.Collection;

public class P2PHostsDetector {

    private final int destinationDiversityThreshold;

    public P2PHostsDetector(int destinationDiversityThreshold) {
        this.destinationDiversityThreshold = destinationDiversityThreshold;
    }

    public P2PHostsDetector(Params params) {
        this(params.destinationDiversityThreshold);
    }

    public P2PHostsDetector() {
        this(Params.getDefault());
    }

    public void flagP2PHosts(Collection<Host> hosts) {
        for (Host host : hosts) {
            for(CommunicationCluster c : host.getCommunicationClusters()) {
                if(c.isP2P()) continue;

                int destinationDiversity = c.getIp16Prefixes().length;
                if(destinationDiversity > destinationDiversityThreshold) {
                    host.setP2P(true);
                    c.setP2P(true);
                }
            }
        }
    }

}
