package main;

import main.common.NetworkFlow;
import main.model.DetectionData;
import test.dataset.BotnetDataset;
import test.dataset.CustomBotnetDataset;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static java.lang.Boolean.parseBoolean;
import static java.lang.Float.isNaN;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import static main.common.ProtocolUtils.convertProtocol;

public class Main {
    private static String networkFlowsPath;
    private static int networkFlowsNumber = -1;
    private static String maliciousIpsPath = null;
    private static String fieldIndexesString = "0,1,2,4,3";
    private static int chunkSize = 5000000;
    private static String logFilePath = null;


    private static final Params params = getDefaultMainParams();

    private static final String USAGE_MESSAGE = "usage: botnetdetection.jar\n" +
            "    -f <networkFlowsPath>\n" +
            "    [-c <flowsChunkSize>] (default: " + chunkSize + ")\n" +
            "    [-n <networkFlowsNumber>] ('-1' means read all) (default: " + networkFlowsNumber + ")\n" +
            "    [-m <maliciousIpsFilePath>] (if not given, real bot ips won't be compared)\n" +
            "    [-i <fieldIndexesString>] (default: " + fieldIndexesString + ")\n" +
            "     - (fieldIndexesString format: 'ipSrcIndex,ipDstIndex,protoIndex,bppInIndex,bppOutIndex') (default: '" + fieldIndexesString + "')\n" +
            "    [-logFilePath <file where to log execution times>] (if not given, no log will be created)\n" +
            "    [-DDThreshold <ddr threshold for p2p hosts detecting>] (default: " + params.destinationDiversityThreshold + ")\n" +
            "    [-MCRThreshold <ddr threshold for p2p hosts detecting>] (default: " + params.mutualContactRatioThreshold + ")\n" +
            "    [-avgDDRThreshold <avgddr threshold for botnet communities flagging>] (default:" + params.averageDestinationDiversityRatioThreshold + ")\n" +
            "    [-avgMCRThreshold <avgmcr threshold for botnet communities flagging>] (default:" + params.averageMutualContactRatioThreshold + ")\n" +
            "    [-lighterAvgMCR <true | false>] (default: " + params.useLighterAvgMCR + ")\n" +
            "    [-useDDRAsNodeWeight <true, false>] (default: true)\n" +
            "    [-nodeEdgeWeightMultiplier <multiplier>] (default: " + params.nodeEdgeWeightMultiplier + ")\n" +
            "    [-logCommunitiesData <true | false>] (default: " + params.logCommunitiesData + ")\n" +
            "    [-logHostsDDData <multiplier>] (default: " + params.logHostsDDData + ")\n" +
            "    [-logCommunitiesMCRs <true | false>] (default: " + params.logCommunitiesMCRs + ")\n";


    private static Function<String, NetworkFlow> extractNetworkFlowForNonAdjustedDataset = string -> {
        try {
            String[] tokens = string.split(",");
            String ipSrc = tokens[0];
            String ipDst = tokens[1];
            String protocol = tokens[2];

            float bppIn = parseFloat(tokens[3]) / parseFloat(tokens[5]);
            bppIn = isNaN(bppIn) ? 0f : bppIn;

            float bppOut = parseFloat(tokens[4]) / parseFloat(tokens[6]);
            bppOut = isNaN(bppOut) ? 0f : bppOut;

            return new NetworkFlow(ipSrc, ipDst, convertProtocol(protocol), (int) bppIn, (int) bppOut);
        } catch (Exception ex) {
            return null;
        }
    };

    private static Function<String, NetworkFlow> extractNetworkFlowForAdjustedDataset = string -> {
        try {
            String[] tokens = string.split(",");
            String ipSrc = tokens[0];
            String ipDst = tokens[1];
            String protocol = tokens[2];
            float bppIn = parseFloat(tokens[4]);
            float bppOut = parseFloat(tokens[3]);

            return new NetworkFlow(ipSrc, ipDst, convertProtocol(protocol), (int) bppIn, (int) bppOut);
        } catch (Exception ex) {
            return null;
        }
    };

    public static void main(String[] args) throws IOException {
        try {
            parseArgs(args);
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
            System.err.println(USAGE_MESSAGE);
            return;
        }

        if (!areArgsComplete()) {
            System.err.println(USAGE_MESSAGE);
            return;
        }

        BufferedWriter logFileWriter = null;
        if (logFilePath != null) {
            logFileWriter = new BufferedWriter(new FileWriter(logFilePath));
            logFileWriter.append("number_of_flows_processed,elapsed_time\n");

            System.out.println("logging to " + logFilePath);
        }

        if (networkFlowsNumber > 5000000 || networkFlowsNumber == -1) {
            System.out.println("WARN: for more than 5000000 network flows you may need to launch with -Xmx option");
        }

        BotnetDataset dataset = new CustomBotnetDataset(networkFlowsPath, maliciousIpsPath, fieldIndexesString);
        int limit = networkFlowsNumber == -1 ? 100000000 : networkFlowsNumber;

        DetectionData detectionData = DetectionData.getEmptyData();
        for (int i = 0; i < limit; i += chunkSize) {
            int nLimit = Math.min(i + chunkSize, limit);
            List<NetworkFlow> networkFlows = dataset.getNetworkFlows(nLimit, i, extractNetworkFlowForAdjustedDataset);

            if (networkFlows.size() == 0) break;

            System.out.println("-----------------------------------------------");
            System.out.println("-----------------------------------------------");
            System.out.println();
            System.out.println("read " + networkFlows.size() + " network flows");
            System.out.println();

            PeerHunter peerHunter = new PeerHunter(params, detectionData);

            long startTime = System.currentTimeMillis();
            detectionData = peerHunter.computeDetectionData(networkFlows);
            long endTime = System.currentTimeMillis();

            long elapsedTime = endTime - startTime;

            if (logFileWriter != null) {
                String logLine = networkFlows.size() + "," + elapsedTime + "\n";
                logFileWriter.write(logLine);
            }

            System.out.println();
            System.out.println("processed " + networkFlows.size() + " in: " + elapsedTime + " millis");
            System.out.println();
            System.out.println("-----------------------------------------------");
            System.out.println("-----------------------------------------------");
        }

        Set<String> extractedBotIps = detectionData.getBotIps();

        if(logFileWriter != null) {
            logFileWriter.flush();
        }

        if (maliciousIpsPath != null) {
            Set<String> currentBotIps = dataset.getMaliciousHostsIps();
            System.out.println("expected bot ips size: " + currentBotIps.size());
            System.out.println("expected bot ips: " + currentBotIps);
        }

        System.out.println("extracted bot ips size: " + extractedBotIps.size());
        System.out.println("extracted bot ips: " + extractedBotIps);
    }


    private static void parseArgs(String[] args) {
        if (args.length % 2 != 0) throw new IllegalArgumentException("invalid args set");

        for (int i = 0; i < args.length; i += 2) {
            String paramType = args[i];
            String value = args[i + 1];

            switch (paramType) {
                case "-f":
                    networkFlowsPath = value;
                    break;
                case "-c":
                    chunkSize = parseInt(value);
                    break;
                case "-n":
                    networkFlowsNumber = parseInt(value);
                    break;
                case "-m":
                    maliciousIpsPath = value;
                    break;
                case "-i":
                    fieldIndexesString = value;
                    break;
                case "-DDThreshold":
                    params.destinationDiversityThreshold = parseInt(value);
                    break;
                case "-MCRThreshold":
                    params.mutualContactRatioThreshold = parseInt(value);
                    break;
                case "-avgDDRThreshold":
                    params.averageDestinationDiversityRatioThreshold = parseFloat(value);
                    break;
                case "-avgMCRThreshold":
                    params.averageMutualContactRatioThreshold = parseFloat(value);
                    break;
                case "-lighterAvgMCR":
                    params.useLighterAvgMCR = parseBoolean(value.toLowerCase());
                    break;
                case "-useDDRAsNodeWeight":
                    boolean use = parseBoolean(value.toLowerCase());
                    if (use) params.setStandardNodeWeightExtractionFunction();
                    else params.setSimpleNodeWeightExtractionFunction();
                    break;
                case "-nodeEdgeWeightMultiplier":
                    params.nodeEdgeWeightMultiplier = parseFloat(value);
                    break;
                case "-logCommunitiesData":
                    params.logCommunitiesData = parseBoolean(value);
                    break;
                case "-logHostsDDData":
                    params.logHostsDDData = parseBoolean(value);
                    break;
                case "-logCommunitiesMCRs":
                    params.logCommunitiesMCRs = parseBoolean(value);
                    break;
                case "-logFilePath":
                    logFilePath = value;
                    break;
                default:
                    throw new IllegalArgumentException("unrecognized option: '" + paramType + "'");
            }
        }
    }

    private static boolean areArgsComplete() {
        return networkFlowsPath != null;
    }

    private static Params getDefaultMainParams() {
        Params result = new Params();

        result.logHostsDDData = true;
        result.logCommunitiesData = true;
        result.logCommunitiesMCRs = true;

        return result;
    }
}
