package test.model;

import main.PeerHunter;
import main.common.CommunicationCluster;
import main.common.Host;
import main.model.DetectionData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import test.dataset.BotnetDataset;
import test.dataset.PeerRushDataset;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.*;

public class DetectionDataTest {
    static DetectionData detectionData;

    @BeforeAll
    static void setup() throws IOException {
        BotnetDataset dataset = new PeerRushDataset();
        detectionData = new PeerHunter().computeDetectionData(dataset.getNetworkFlows());
    }

    @Test
    void serialization_and_deserialization_time() throws IOException, ClassNotFoundException {
        long startTime = System.currentTimeMillis();
        byte[] serialized = DetectionData.serializeData(detectionData);

        long serializationTime = System.currentTimeMillis() - startTime;
        DetectionData.deserializeData(serialized);
        long deserializationTime = System.currentTimeMillis() - (startTime + serializationTime);

        assertTrue(serializationTime < 1000);
        assertTrue(deserializationTime < 500);
    }

    @Test
    void serialization_and_deserialization_work_correctly() throws IOException, ClassNotFoundException {
        byte[] serialized = DetectionData.serializeData(detectionData);
        DetectionData deserialized = DetectionData.deserializeData(serialized);

        assertEquals(detectionData.getGraph(), deserialized.getGraph());
        assertEquals(detectionData.getBotIps(), deserialized.getBotIps());
        assertEquals(detectionData.getHosts(), deserialized.getHosts());

        List<CommunicationCluster[]> expectedClustersLists = detectionData.getHosts().stream().map(Host::getCommunicationClusters).collect(toList());
        List<CommunicationCluster[]> actualClustersLists = deserialized.getHosts().stream().map(Host::getCommunicationClusters).collect(toList());

        assertEquals(expectedClustersLists.size(), actualClustersLists.size());

        for (int i = 0; i < expectedClustersLists.size(); i++) {
            CommunicationCluster[] expectedClusters = expectedClustersLists.get(i);
            CommunicationCluster[] actualClusters = actualClustersLists.get(i);

            assertArrayEquals(expectedClusters, actualClusters);

            List<int[]> expectedClustersContacts = Arrays.stream(expectedClusters).map(CommunicationCluster::getContacts).collect(toList());
            List<int[]> actualClustersContacts = Arrays.stream(actualClusters).map(CommunicationCluster::getContacts).collect(toList());

            assertEquals(expectedClustersContacts.size(), actualClustersContacts.size());

            for (int j = 0; j < expectedClustersContacts.size(); j++) {
                int[] expectedClusterContacts = expectedClustersContacts.get(j);
                int[] actualClusterContacts = actualClustersContacts.get(j);

                assertArrayEquals(expectedClusterContacts, actualClusterContacts);
            }

            List<Boolean> expectedClustersIsP2PFlags = Arrays.stream(expectedClusters).map(CommunicationCluster::isP2P).collect(toList());
            List<Boolean> actualClustersIsP2PFlags = Arrays.stream(actualClusters).map(CommunicationCluster::isP2P).collect(toList());

            assertEquals(expectedClustersIsP2PFlags, actualClustersIsP2PFlags);
        }
    }


}
