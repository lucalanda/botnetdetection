package test.common;

import org.junit.jupiter.api.Test;

import java.util.Collection;

import static java.util.stream.IntStream.range;
import static main.common.IPV4Utils.*;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.fixtures.FixturesGenerator.anIpv4String;
import static test.util.Util.buildSet;

class IPV4UtilsTest {

    @Test
    void convert_and_revert_work() {
        Collection<String> ipsToTest = buildSet("192.168.250.76", "10.20.30.40", "10.10.10.10", "6.1.222.123", "0.0.0.0", "255.255.255.255");
        range(0, 1000).forEach(_i -> ipsToTest.add(anIpv4String()));

        ipsToTest.forEach(ip -> {
            int converted = convertIP(ip);
            String reverted = revertIP(converted);

            assertEquals(ip, reverted);
        });
    }

    @Test
    void ip16Prefix_works() {
        int ip1 = convertIP("192.168.10.10");
        int ip2 = convertIP("192.168.20.20");
        int ip3 = convertIP("192.168.35.45");
        int ip4 = convertIP("10.20.35.45");
        int ip5 = convertIP("10.20.35.45");


        assertEquals(ip16Prefix(ip1), ip16Prefix(ip2));
        assertEquals(ip16Prefix(ip2), ip16Prefix(ip3));
        assertEquals(ip16Prefix(ip4), ip16Prefix(ip5));
        assertNotEquals(ip16Prefix(ip1), ip16Prefix(ip4));
    }
}
