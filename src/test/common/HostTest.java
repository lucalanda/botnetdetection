package test.common;

import gnu.trove.set.TShortSet;
import gnu.trove.set.hash.TIntHashSet;
import main.common.CommunicationCluster;
import main.common.Host;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toSet;
import static main.common.IPV4Utils.convertIP;
import static main.common.IPV4Utils.convertedIpv4Set;
import static main.common.ProtocolUtils.convertProtocol;
import static main.common.Utils.destinationDiversityRatio;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.util.Util.buildArray;
import static test.util.Util.buildSet;

public class HostTest {

    private static Set<String> p2pContacts;
    private static CommunicationCluster[] communicationClusters;
    private static ArrayList<CommunicationCluster> communicationClustersList;
    private static Host host;

    @BeforeEach
    void setUp() {
        p2pContacts = buildSet("123.0.0.1", "125.2.1.0", "120.1.9.8", "123.1.2.1", "125.2.0.6");

        communicationClusters = buildArray(
                new CommunicationCluster(convertedIpv4Set("123.0.0.1", "125.2.1.0", "120.1.9.8").toArray(), convertProtocol("tcp"), 100, 100, true),
                new CommunicationCluster(convertedIpv4Set("123.1.2.1", "123.0.100.100").toArray(), convertProtocol("tcp"), 50, 50, true),
                new CommunicationCluster(convertedIpv4Set("125.2.0.6").toArray(), convertProtocol("tcp"), 10, 10, false)
        );

        communicationClustersList = new ArrayList<>(Arrays.asList(communicationClusters));

        host = new Host(false, convertIP("127.0.0.1"), communicationClusters);
    }

    @Test
    void it_calculates_own_destinationDiversityRatio_correctly() {
        float actual = host.getDestinationDiversityRatio();
        float expected = destinationDiversityRatio(convertIP(p2pContacts));

        assertEquals(expected, actual);
    }

    @Test
    void update_updates_correctly() {
        int oldHashCode = host.realHashCode();

        Set<CommunicationCluster> newCommunicationClusters = buildSet(
                new CommunicationCluster(convertedIpv4Set("192.168.0.0"), convertProtocol("tcp"), 50, 50),
                new CommunicationCluster(convertedIpv4Set("192.168.1.1"), convertProtocol("tcp"), 10, 10),
                new CommunicationCluster(convertedIpv4Set("10.20.30.40"), convertProtocol("udp"), 25, 25)
        );

        host.update(newCommunicationClusters);

        Set<CommunicationCluster> finalCommunicationClustersSet = buildSet(
                new CommunicationCluster(convertedIpv4Set("123.0.0.1", "125.2.1.0", "120.1.9.8").toArray(), convertProtocol("tcp"), 100, 100, true),
                new CommunicationCluster(convertedIpv4Set("123.1.2.1", "123.0.100.100", "192.168.0.0").toArray(), convertProtocol("tcp"), 50, 50, false),
                new CommunicationCluster(convertedIpv4Set("125.2.0.6", "192.168.1.1"), convertProtocol("tcp"), 10, 10),
                new CommunicationCluster(convertedIpv4Set("10.20.30.40"), convertProtocol("udp"), 25, 25)
        );

        assertEquals(finalCommunicationClustersSet, Util.toSet(host.getCommunicationClusters()));
        assertNotEquals(oldHashCode, host.realHashCode());
    }

    //test necessary since CommunicationCluster does not use p2pContacts or ip16Prefixes for equality
    @Test
    void update_updates_communication_clusters_correctly() {
        Set<CommunicationCluster> clusters = buildSet(
                new CommunicationCluster(convertedIpv4Set("123.0.0.1", "125.2.1.0", "120.1.9.8"), convertProtocol("tcp"), 100, 100),
                new CommunicationCluster(convertedIpv4Set("123.1.2.1"), convertProtocol("tcp"), 50, 50),
                new CommunicationCluster(convertedIpv4Set("125.2.0.6"), convertProtocol("tcp"), 10, 10)
        );

        Host host = new Host(false, convertIP("127.0.0.1"), buildArray(clusters));

        Set<CommunicationCluster> newCommunicationClusters = buildSet(
                new CommunicationCluster(convertedIpv4Set("192.168.0.0"), convertProtocol("tcp"), 100, 100),
                new CommunicationCluster(convertedIpv4Set("192.168.1.1"), convertProtocol("tcp"), 50, 50)
        );

        host.update(newCommunicationClusters);

        CommunicationCluster[] actual = host.getCommunicationClusters();
        CommunicationCluster[] expected = buildArray(
                new CommunicationCluster(
                        convertedIpv4Set("123.0.0.1", "125.2.1.0", "120.1.9.8", "192.168.0.0"), convertProtocol("tcp"), 100, 100
                ),
                new CommunicationCluster(convertedIpv4Set("123.1.2.1", "192.168.1.1"), convertProtocol("tcp"), 50, 50),
                new CommunicationCluster(convertedIpv4Set("125.2.0.6"), convertProtocol("tcp"), 10, 10)
        );

        assertArrayEquals(actual, expected);

        Set<int[]> actualHostContacts = Arrays.stream(actual).map(CommunicationCluster::getContacts).collect(toSet());
        Set<int[]> expectedHostContacts = Arrays.stream(actual).map(CommunicationCluster::getContacts).collect(toSet());

        assertEquals(actualHostContacts, expectedHostContacts);

        Set<TShortSet> actualIP16Prefixes = Arrays.stream(actual).map(CommunicationCluster::getIp16Prefixes).map(Util::toSet).collect(toSet());
        Set<TShortSet> expectedIP16Prefixes = Arrays.stream(actual).map(CommunicationCluster::getIp16Prefixes).map(Util::toSet).collect(toSet());

        assertEquals(actualIP16Prefixes, expectedIP16Prefixes);
    }

    @Test
    void update_does_not_update_with_empty_sets() {
        int oldHashCode = host.realHashCode();

        host.update(emptySet());

        assertEquals(oldHashCode, host.realHashCode());
    }

    @Test
    void update_changes_hashcode_when_contacts_number_changes() {
        CommunicationCluster cluster = new CommunicationCluster(convertedIpv4Set("10.10.10.10").toArray(), convertProtocol("tcp"), 100, 100, true);

        int oldClustersNumber = host.getCommunicationClusters().length;
        int oldHashCode = host.realHashCode();

        host.update(buildSet(cluster));

        int newClustersNumber = host.getCommunicationClusters().length;
        int newHashCode = host.realHashCode();

        assertEquals(oldClustersNumber, newClustersNumber);

        assertNotEquals(oldHashCode, newHashCode);
    }

    @Test
    void getP2PCommunicationClusters_returns_only_p2p_clusters() {
        CommunicationCluster cluster1 = communicationClustersList.get(0);
        CommunicationCluster cluster2 = communicationClustersList.get(1);
        CommunicationCluster cluster3 = communicationClustersList.get(2);

        cluster1.setP2P(true);
        cluster2.setP2P(true);
        cluster3.setP2P(false);

        Set<CommunicationCluster> expected = buildSet(cluster1, cluster2);
        assertEquals(expected, host.getP2PCommunicationClusters());
    }

    @Test
    void getP2PContacts_returns_only_correct_contacts_from_p2p_clusters() {
        communicationClustersList.get(0).setP2P(true);
        communicationClustersList.get(1).setP2P(false);
        communicationClustersList.get(2).setP2P(true);

        TIntHashSet expected = convertedIpv4Set("123.0.0.1", "125.2.1.0", "120.1.9.8", "125.2.0.6");
        TIntHashSet actual = host.getP2PContacts();

        assertEquals(expected, actual);
    }
}
