package test.common;

import gnu.trove.set.hash.THashSet;
import main.HostsExtractor;
import main.common.CommunicationCluster;
import main.common.Host;
import main.common.NetworkFlow;
import org.junit.jupiter.api.Test;

import java.util.*;

import static java.util.Collections.emptyList;
import static junit.framework.TestCase.assertSame;
import static junit.framework.TestCase.assertTrue;
import static main.common.IPV4Utils.convertIP;
import static main.common.IPV4Utils.convertedIpv4Set;
import static main.common.ProtocolUtils.convertProtocol;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.util.Util.*;

class HostsExtractorTest {

    HostsExtractor extractor = new HostsExtractor();

    @Test
    void extractHosts() {
        List<NetworkFlow> networkFlows = Arrays.asList(
                new NetworkFlow("10.10.1.2", "10.10.1.3", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("10.10.1.2", "10.10.2.4", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("10.10.1.2", "10.13.2.1", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("10.10.1.2", "10.11.1.4", convertProtocol("udp"), 10, 10),
                new NetworkFlow("10.10.1.2", "10.12.2.1", convertProtocol("tcp"), 100, 10),
                new NetworkFlow("20.20.0.0", "10.10.1.3", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("20.20.0.0", "10.10.1.4", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("20.20.0.0", "10.10.0.0", convertProtocol("tcp"), 1000, 1000)
        );

        ArrayList<Host> hosts = extractor.extractUpdatedHostsList(networkFlows);
        assertEquals(2, hosts.size());

        Host host1 = hosts.get(0);
        assertFalse(host1.isP2P());
        assertEquals(convertIP("10.10.1.2"), host1.getIp());

        CommunicationCluster[] expectedHost1Clusters = buildArray(
                new CommunicationCluster(convertedIpv4Set("10.10.1.3", "10.13.2.1"), convertProtocol("tcp"), 10, 10),
                new CommunicationCluster(convertedIpv4Set("10.11.1.4"), convertProtocol("udp"), 10, 10),
                new CommunicationCluster(convertedIpv4Set("10.12.2.1"), convertProtocol("tcp"), 100, 10)
        );

        assertEquals(toSet(expectedHost1Clusters), toSet(host1.getCommunicationClusters()));

        Host host2 = hosts.get(1);
        assertFalse(host2.isP2P());
        assertEquals(convertIP("20.20.0.0"), host2.getIp());

        CommunicationCluster[] expectedHost2Clusters = buildArray(
                new CommunicationCluster(convertedIpv4Set("10.10.1.3"), convertProtocol("tcp"), 10, 10),
                new CommunicationCluster(convertedIpv4Set("10.10.2.4"), convertProtocol("tcp"), 1000, 1000)
        );

        assertEquals(toSet(expectedHost2Clusters), toSet(host2.getCommunicationClusters()));
    }

    @Test
    void extractHosts_works_with_empty_collection() {
        ArrayList<Host> hosts = new HostsExtractor().extractUpdatedHostsList(emptyList());

        assertEquals(emptyList(), hosts);
    }

    @Test
    void extractHost_execution_time() {
        Collection<NetworkFlow> networkFlows = getRandomNetworkFlows(100000);

        long startTime = new Date().getTime();

        extractor.extractUpdatedHostsList(networkFlows);

        long totalMilliseconds = new Date().getTime() - startTime;

        assertTrue(totalMilliseconds < 2000);
    }

    @Test
    void extractHosts_updates_current_hosts_correctly() {
        Random random = new Random(42);

        Collection<NetworkFlow> networkFlows = getRandomNetworkFlows(1000);

        ArrayList<Host> hosts = extractor.extractUpdatedHostsList(networkFlows);

        Host host1 = hosts.get(random.nextInt(networkFlows.size()));
        Host host2 = hosts.get(random.nextInt(networkFlows.size()));

        // new data to add to hosts
        CommunicationCluster[] newHost1CommunicationClusters = buildArray(
                new CommunicationCluster(convertedIpv4Set("00.01.02.03", "04.05.06.07"), convertProtocol("tcp"), 10000, 10000)
        );
        CommunicationCluster[] newHost2CommunicationClusters = buildArray(
                new CommunicationCluster(convertedIpv4Set("08.09.010.011"), convertProtocol("tcp"), 100000, 100000),
                new CommunicationCluster(convertedIpv4Set("012.013.14.15"), convertProtocol("udp"), 20000, 20000)
        );

        Host initializationHost1 = new Host(host1.isP2P(), host1.getIp(), newHost1CommunicationClusters);
        Host initializationHost2 = new Host(host2.isP2P(), host2.getIp(), newHost2CommunicationClusters);

        // backup data for comparison after extraction
        int oldHost1HashCode = initializationHost1.realHashCode();
        int oldHost2HashCode = initializationHost2.realHashCode();
        Set<CommunicationCluster> oldHost1CommunicationClusters = toSet(initializationHost1.getCommunicationClusters());
        Set<CommunicationCluster> oldHost2CommunicationClusters = toSet(initializationHost2.getCommunicationClusters());

        ArrayList<Host> initializationHosts = new ArrayList<>(Arrays.asList(initializationHost1, initializationHost2));

        ArrayList<Host> newHostList = new HostsExtractor(initializationHosts).extractUpdatedHostsList(networkFlows);

        assertSame(newHostList.get(0), initializationHost1);
        assertSame(newHostList.get(1), initializationHost2);

        assertNotEquals(oldHost1HashCode, initializationHost1.realHashCode());
        assertNotEquals(oldHost1CommunicationClusters, initializationHost1.getCommunicationClusters());

        assertTrue(toSet(initializationHost1.getCommunicationClusters()).containsAll(oldHost1CommunicationClusters));

        assertNotEquals(oldHost2HashCode, initializationHost2.realHashCode());
        assertNotEquals(oldHost2CommunicationClusters, initializationHost2.getCommunicationClusters());

        assertTrue(toSet(initializationHost2.getCommunicationClusters()).containsAll(oldHost2CommunicationClusters));
    }

    private <T> Set<T> cloneSet(Set<T> set) {
        return new HashSet<>(set);
    }
}