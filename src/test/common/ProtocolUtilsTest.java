package test.common;

import org.junit.jupiter.api.Test;

import static junit.framework.TestCase.assertTrue;
import static main.common.ProtocolUtils.*;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProtocolUtilsTest {

    @Test
    void convert_works_correctly() {
        assertEquals((byte) 0, convertProtocol("tcp"));
        assertEquals((byte) 1, convertProtocol("udp"));
        assertEquals((byte) 2, convertProtocol("icmp"));

        assertEquals((byte) 0, convertProtocol("TcP"));

        assertThrows(IllegalArgumentException.class, () -> convertProtocol("man"));
    }

    @Test
    void revert_works_correctly() {
        assertEquals("tcp", revertProtocol((byte) 0));
        assertEquals("udp", revertProtocol((byte) 1));
        assertEquals("icmp", revertProtocol((byte) 2));


        assertThrows(IllegalArgumentException.class, () -> revertProtocol((byte) 4));
    }

    @Test
    void isProtocolAccepted_works() {
        assertTrue(isProtocolAccepted("tcp"));
        assertTrue(isProtocolAccepted("udp"));
        assertTrue(isProtocolAccepted("icmp"));

        assertTrue(isProtocolAccepted("TcP"));

        assertFalse(isProtocolAccepted("man"));

        assertTrue(isProtocolAccepted((byte) 0));
        assertTrue(isProtocolAccepted((byte) 1));
        assertTrue(isProtocolAccepted((byte) 2));

        assertFalse(isProtocolAccepted((byte) 3));
    }
}
