package test.common;

import main.common.Couple;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CoupleTest {

    @Test
    void equalsWithInvertedParameters() {
        assertEquals(new Couple<>("0", "1"), new Couple<>("0", "1"));
        assertEquals(new Couple<>("0", "1"), new Couple<>("1", "0"));
    }

}