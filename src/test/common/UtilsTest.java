package test.common;

import gnu.trove.set.hash.TIntHashSet;
import main.common.Couple;
import main.common.Utils;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static main.common.IPV4Utils.convertedIpv4Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static test.util.Assertions.raises;
import static test.util.Util.buildSet;

class UtilsTest {

    @Test
    void ip16Prefix() {
        assertEquals("10.10", Utils.ip16Prefix("10.10.2.1"));
        assertEquals("192.168", Utils.ip16Prefix("192.168.3.54"));
        assert (raises(() -> Utils.ip16Prefix("a1:a2:c:b1:b2")));
        assert (raises(() -> Utils.ip16Prefix(null)));
//        assertEquals("a1", Utils.ipv416Prefix("a1:a2:c:b1:b2"));
//        assert (raises(() -> Utils.ipv416Prefix(null)));
    }

    @Test
    void destinationDiversity() {
        TIntHashSet ips = convertedIpv4Set("10.10.1.2", "10.10.1.3", "10.1.2.3", "20.20.1.3");

        assertEquals(3, Utils.destinationDiversity(ips));
    }

    @Test
    void destinationDiversityRatio() {
        TIntHashSet ipList = convertedIpv4Set("10.10.1.2", "10.10.1.3", "10.1.2.3", "20.20.1.3");

        assertEquals(0.75, Utils.destinationDiversityRatio(ipList));
    }

    @Test
    void mutualContactRatioTest() {
        TIntHashSet set1 = convertedIpv4Set("10.10.1.2", "10.10.2.3", "10.10.3.4", "10.10.4.5", "10.10.5.6");
        TIntHashSet set2 = convertedIpv4Set("10.10.1.2", "10.10.2.3", "10.10.3.4", "10.10.8.9", "10.10.9.0");

        float expected = 0.42f;
        assertEquals(expected, Utils.mutualContactRatio(set1, set2), 0.01);
    }

    @Test
    void setsHaveIntersection() {
        Set<String> set1 = buildSet("0", "1", "2");
        Set<String> set2 = buildSet("1", "3");
        Set<String> set3 = buildSet("2", "4");

        assert(Utils.setsHaveIntersection(set1, set2));
        assert(Utils.setsHaveIntersection(set1, set3));
        assertFalse(Utils.setsHaveIntersection(set2, set3));
    }

    @Test
    void getCouples() {
        Set<String> set = buildSet("0", "1", "2");
        Set<Couple<String>> expected = buildSet(new Couple<>("0", "1"), new Couple<>("1", "2"), new Couple<>("0", "2"));

        assertEquals(expected, Utils.getCouples(set));
    }
}
