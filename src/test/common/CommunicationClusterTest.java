package test.common;

import gnu.trove.set.hash.TIntHashSet;
import gnu.trove.set.hash.TShortHashSet;
import main.common.CommunicationCluster;
import main.common.Utils;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static junit.framework.TestCase.assertTrue;
import static main.common.IPV4Utils.convertPrefixes;
import static main.common.IPV4Utils.convertedIpv4Set;
import static main.common.ProtocolUtils.convertProtocol;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static test.util.Util.buildSet;
import static test.util.Util.toSet;

public class CommunicationClusterTest {

    @Test
    void clusters_sets_intersection() {
        TIntHashSet contactSet1 = convertedIpv4Set("10.10.1.3", "10.10.1.4");
        TIntHashSet contactSet2 = convertedIpv4Set("1.2.3.4", "5.213.2.3");
        TIntHashSet contactSet3 = convertedIpv4Set("10.10.1.2", "10.10.1.5");

        CommunicationCluster communicationCluster1 = new CommunicationCluster(contactSet1.toArray(), convertProtocol("tcp"), 0, 0, false);
        CommunicationCluster communicationCluster2 = new CommunicationCluster(contactSet2.toArray(), convertProtocol("tcp"), 0, 0, false);
        CommunicationCluster communicationCluster3 = new CommunicationCluster(contactSet2.toArray(), convertProtocol("tcp"), 0, 0, true);
        CommunicationCluster communicationCluster4 = new CommunicationCluster(contactSet3.toArray(), convertProtocol("udp"), 0, 0, false);

        Set<CommunicationCluster> set1 = buildSet(communicationCluster1, communicationCluster4);
        Set<CommunicationCluster> set2 = buildSet(communicationCluster2);
        Set<CommunicationCluster> set3 = buildSet(communicationCluster4);
        Set<CommunicationCluster> set4 = buildSet(communicationCluster3, communicationCluster4);
        Set<CommunicationCluster> set5 = buildSet(communicationCluster4, communicationCluster2);

        assertTrue(Utils.setsHaveIntersection(set1, set1));
        assertTrue(Utils.setsHaveIntersection(set1, set2));
        assertTrue(Utils.setsHaveIntersection(set2, set5));
        assertTrue(Utils.setsHaveIntersection(set1, set5));
        assertFalse(Utils.setsHaveIntersection(set2, set3));
        assertTrue(Utils.setsHaveIntersection(set4, set2));
    }

    @Test
    void it_computes_its_own_ip16prefixes_set() {
        TIntHashSet contactSet1 = convertedIpv4Set("10.10.1.2", "10.10.3.4", "10.20.5.6", "20.20.0.0", "5.6.1.2");
        TIntHashSet contactSet2 = convertedIpv4Set("10.10.1.2", "10.11.3.4", "3.4.5.6", "3.4.0.0");

        CommunicationCluster cluster1 = new CommunicationCluster(contactSet1.toArray(), convertProtocol("tcp"), 0, 0, false);
        CommunicationCluster cluster2 = new CommunicationCluster(contactSet2.toArray(), convertProtocol("tcp"), 0, 0, false);

        assertEquals(convertPrefixes(buildSet("10.10", "10.20", "20.20", "5.6")), toSet(cluster1.getIp16Prefixes()));
        assertEquals(convertPrefixes(buildSet("10.10", "10.11", "3.4")), toSet(cluster2.getIp16Prefixes()));
    }

    @Test
    void updatedInstance_returns_updated_instance_correctly() {
        TIntHashSet contactSet = convertedIpv4Set("10.10.1.2", "10.10.3.4", "10.20.5.6");

        CommunicationCluster cluster = new CommunicationCluster(contactSet.toArray(), convertProtocol("tcp"), 0, 0, false);

        assertEquals(contactSet, toSet(cluster.getContacts()));
        assertEquals(convertPrefixes(buildSet("10.10","10.20")), toSet(cluster.getIp16Prefixes()));

        TIntHashSet newContacts = convertedIpv4Set("20.20.0.0", "5.6.1.2");

        CommunicationCluster updated = cluster.updatedInstance(newContacts.toArray());

        TIntHashSet expectedContacts = convertedIpv4Set("10.10.1.2", "10.10.3.4", "10.20.5.6", "20.20.0.0", "5.6.1.2");
        TShortHashSet expectedIP16Prefixes = convertPrefixes(buildSet("10.10", "10.20", "20.20", "5.6"));

        assertEquals(expectedContacts, toSet(updated.getContacts()));
        assertEquals(expectedIP16Prefixes, toSet(updated.getIp16Prefixes()));
    }
}
