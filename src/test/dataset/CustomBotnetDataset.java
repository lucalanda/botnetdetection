package test.dataset;

import main.common.NetworkFlow;

import static java.lang.Integer.parseInt;
import static main.common.ProtocolUtils.convertProtocol;

public class CustomBotnetDataset extends BotnetDataset {

    private int ipSrcIndex;
    private int ipDstIndex;
    private int protocolIndex;
    private int bppInIndex;
    private int bppOutIndex;

    public CustomBotnetDataset(String flowsFilePath, String maliciousIpsFilePath, String fieldsIndexesSpec) {
        super(flowsFilePath, maliciousIpsFilePath);

        String[] indexes = fieldsIndexesSpec.split(",");
        if (!isIndexesFormatCorrect(indexes))
            throw new IllegalArgumentException("wrong fieldsIndexesSpec format, expected format example: '0,1,2,4,3'");

        ipSrcIndex = parseInt(indexes[0]);
        ipDstIndex = parseInt(indexes[1]);
        protocolIndex = parseInt(indexes[2]);
        bppInIndex = parseInt(indexes[3]);
        bppOutIndex = parseInt(indexes[4]);
    }

    public CustomBotnetDataset(String flowsFilePath, String maliciousIpsFilePath) {
        this(flowsFilePath, maliciousIpsFilePath, "0,1,2,4,3");
    }

    @Override
    public NetworkFlow getNetworkFlowFromLine(String line) throws IllegalArgumentException {
        String[] tokens = line.split(",");
        try {
            return new NetworkFlow(tokens[ipSrcIndex], tokens[ipDstIndex], convertProtocol(tokens[protocolIndex]), parseInt(tokens[bppInIndex]), parseInt(tokens[bppOutIndex]));
        } catch (ArrayIndexOutOfBoundsException aex) {
            throw aex;
        } catch (Exception ex) {
            return null;
        }
    }

    private boolean isIndexesFormatCorrect(String[] indexes) {
        if (indexes.length != 5) return false;

        for (String index : indexes) {
            if (!isInteger(index)) return false;
        }

        return true;
    }

    private boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }

        return true;
    }
}
