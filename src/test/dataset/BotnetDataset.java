package test.dataset;

import main.common.NetworkFlow;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.toSet;
import static test.util.Util.getFileLines;

public abstract class BotnetDataset {

    private final String flowsFilePath;
    private final String maliciousIpsFilePath;

    public BotnetDataset(String flowsFilePath, String maliciousIpsFilePath) {
        this.flowsFilePath = flowsFilePath;
        this.maliciousIpsFilePath = maliciousIpsFilePath;
    }

    public List<NetworkFlow> getNetworkFlows() throws IOException {
        return getNetworkFlows(-1);
    }

    public List<NetworkFlow> getNetworkFlows(int nLimit) throws IOException {
        return getNetworkFlows(nLimit, 0);
    }

    public List<NetworkFlow> getNetworkFlows(int nLimit, int offset) throws IOException {
        return this.getNetworkFlows(nLimit, offset, this::getNetworkFlowFromLine);
    }

    public List<NetworkFlow> getNetworkFlows(int nLimit, int offset, Function<String, NetworkFlow> extractNetworkFlowFunction) throws IOException {
        Collection<String> fileLines = getFileLines(flowsFilePath, nLimit, offset);

        List<NetworkFlow> result = new ArrayList<>(fileLines.size());

        for(String line : fileLines) {
            NetworkFlow networkFlow = extractNetworkFlowFunction.apply(line);
            if(networkFlow != null) result.add(networkFlow);
        }

        System.err.println("...Number of unparsed flows: " + (fileLines.size() - result.size()));
        return result;
    }

    public Set<String> getMaliciousHostsIps() throws IOException {
        return getFileLines(maliciousIpsFilePath).stream()
                .filter(line -> !line.equals(""))
                .collect(toSet());
    }

    abstract NetworkFlow getNetworkFlowFromLine(String line) throws IllegalArgumentException;

}
