package test.dataset;

import main.common.NetworkFlow;

import static java.lang.Integer.parseInt;
import static main.common.ProtocolUtils.convertProtocol;

public class UNSWDatasetSubset extends BotnetDataset {

    private final static String FLOWS_FILE_PATH = "src/test/dataset/unsw_2018_iot_botnet_subset/flows.csv";
    private final static String MALICIOUS_IPS_FILE_PATH = "src/test/dataset/unsw_2018_iot_botnet_subset/malicious_ips_list.csv";

    public UNSWDatasetSubset() {
        super(FLOWS_FILE_PATH, MALICIOUS_IPS_FILE_PATH);
    }

    @Override
    public NetworkFlow getNetworkFlowFromLine(String line) throws IllegalArgumentException {
        String[] tokens = line.split(",");
        try {
            return new NetworkFlow(tokens[1], tokens[2], convertProtocol(tokens[0]), parseInt(tokens[3]), parseInt(tokens[4]));
        } catch (Exception ex) {
            return null;
        }
    }
}
