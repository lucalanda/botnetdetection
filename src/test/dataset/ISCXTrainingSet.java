package test.dataset;

import main.common.NetworkFlow;

import static java.lang.Integer.parseInt;
import static main.common.ProtocolUtils.convertProtocol;

public class ISCXTrainingSet extends BotnetDataset {

    private final static String FLOWS_FILE_PATH = "src/test/dataset/iscx_training/flows.csv";
    private final static String MALICIOUS_IPS_FILE_PATH = "src/test/dataset/iscx_training/malicious_ips_list.csv";

    public ISCXTrainingSet() {
        super(FLOWS_FILE_PATH, MALICIOUS_IPS_FILE_PATH);
    }

    @Override
    public NetworkFlow getNetworkFlowFromLine(String line) throws IllegalArgumentException {
        String[] tokens = line.split(",");
        try {
            return new NetworkFlow(tokens[0], tokens[1], convertProtocol(tokens[2]), parseInt(tokens[3]), parseInt(tokens[4]));
        } catch (Exception ex) {
            return null;
        }
    }

}
