package test.util;

import gnu.trove.set.hash.THashSet;
import gnu.trove.set.hash.TIntHashSet;
import gnu.trove.set.hash.TShortHashSet;
import main.common.CommunicationCluster;
import main.common.NetworkFlow;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static test.fixtures.FixturesGenerator.*;

public class Util {
    public static <T> Set<T> buildSet(T... params) {
        ArrayList<T> paramsList = new ArrayList<>(params.length);
        for (T param : params) paramsList.add(param);

        return new HashSet<>(paramsList);
    }

    public static <T> T[] buildArray(T... params) {
        return params;
    }

    public static Set<CommunicationCluster> toSet(CommunicationCluster[] communicationClusters) {
        THashSet<CommunicationCluster> result = new THashSet<>(communicationClusters.length);

        for (CommunicationCluster c : communicationClusters) {
            result.add(c);
        }

        return result;
    }

    public static TIntHashSet toSet(int[] array) {
        return new TIntHashSet(array);
    }

    public static TShortHashSet toSet(short[] array) {
        return new TShortHashSet(array);
    }

    public static CommunicationCluster[] buildArray(Collection<CommunicationCluster> collection) {
        return collection.toArray(new CommunicationCluster[collection.size()]);
    }

    public static double[] doubleArray(double... params) {
        return params;
    }

    public static int[] intArray(int... params) {
        return params;
    }

    public static <T> Set<T> mergeSets(Set<T> firstSet, Set<T> secondSet) {
        Set<T> result = new HashSet<>();

        result.addAll(firstSet);
        result.addAll(secondSet);

        return result;
    }

    public static Collection<String> getFileLines(String filePath) throws IOException {
        return getFileLines(filePath, -1);
    }

    public static Collection<String> getFileLines(String filePath, int rowsLimit) throws IOException {
        return getFileLines(filePath, rowsLimit, 0);
    }

    public static Collection<String> getFileLines(String filePath, int rowsLimit, int offset) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));

        Stream<String> linesStream = reader.lines().skip(offset);

        if(rowsLimit != -1 && rowsLimit - offset > 0)
            linesStream = linesStream.limit(rowsLimit - offset);

        return linesStream.collect(toList());
    }

    public static Collection<NetworkFlow> getRandomNetworkFlows(int nRecords) {
        return range(0, nRecords)
                .mapToObj((_i) -> new NetworkFlow(anIpv4String(), anIpv4String(), aProtocol(), aBPPValue(), aBPPValue()))
                .collect(toList());
    }
}
