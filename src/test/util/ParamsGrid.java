package test.util;

import main.Params;

import java.util.ArrayList;
import java.util.List;

public class ParamsGrid {
    private final int[] desinationDiversityRatioThresholdValues;
    private final float[] mutualContactRatioThresholdValues;
    private final float[] averageDesinationDiversityRatioThresholdValues;
    private final float[] averageMutualContactRatioThresholdValues;

    public List<Params> paramsList = new ArrayList<>();

    public ParamsGrid(int[] desinationDiversityRatioThresholdValues, float[] mutualContactRatioThresholdValues, float[] averageDesinationDiversityRatioThresholdValues, float[] averageMutualContactRatioThresholdValues) {
        this.desinationDiversityRatioThresholdValues = desinationDiversityRatioThresholdValues;
        this.mutualContactRatioThresholdValues = mutualContactRatioThresholdValues;
        this.averageDesinationDiversityRatioThresholdValues = averageDesinationDiversityRatioThresholdValues;
        this.averageMutualContactRatioThresholdValues = averageMutualContactRatioThresholdValues;

        setupParamsList();
    }

    private void setupParamsList() {
        for (int a = 0; a < desinationDiversityRatioThresholdValues.length; a++) {
            for (int b = 0; b < mutualContactRatioThresholdValues.length; b++) {
                for (int c = 0; c < averageDesinationDiversityRatioThresholdValues.length; c++) {
                    for (int d = 0; d < desinationDiversityRatioThresholdValues.length; d++) {
                        paramsList.add(new Params(
                                desinationDiversityRatioThresholdValues[a],
                                mutualContactRatioThresholdValues[b],
                                averageDesinationDiversityRatioThresholdValues[c],
                                averageMutualContactRatioThresholdValues[d]
                        ));
                    }
                }
            }
        }
    }
}
