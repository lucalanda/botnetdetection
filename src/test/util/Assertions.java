package test.util;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.util.Util.buildSet;

public class Assertions {
    public static boolean raises(Runnable runnable) {
        boolean thrown = false;

        try {
            runnable.run();
        } catch (Exception ex) {
            thrown = true;
        }

        return thrown;
    }

    public static <T> void assertArrayEqualsInAnyOrder(T[] first, T[] second) {
        Set<T> firstToSet = buildSet(first);
        Set<T> secondToSet = buildSet(second);

        assertEquals(firstToSet, secondToSet);
    }

}
