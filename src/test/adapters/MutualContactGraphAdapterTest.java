package test.adapters;

import main.adapters.MutualContactGraphAdapter;
import main.common.Couple;
import main.common.Host;
import main.model.MutualContactGraph;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static test.fixtures.FixturesGenerator.anHostWithContacts;
import static test.util.Util.buildSet;

class MutualContactGraphAdapterTest {

    private static final MutualContactGraphAdapter adapter = new MutualContactGraphAdapter();

    private static final Host defaultGraphHost1 = anHostWithContacts("10.20.30.40");
    private static final Host defaultGraphHost2 = anHostWithContacts("50.60.70.80", "10.20.30.40");
    private static final Host defaultGraphHost3 = anHostWithContacts("0.1.2.3", "4.5.6.7");

    private static final ArrayList<Host> hostsArrayList = new ArrayList<>(asList(defaultGraphHost1, defaultGraphHost2, defaultGraphHost3));

    @Test
    void convert() {
        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph = getDefaultGraph();

        MutualContactGraph actual = adapter.convert(graph, hostsArrayList);

        MutualContactGraph expected = new MutualContactGraph(new String[]{"0,1,0.5", "1,2,0.3", "0,2,1.5"});

        assertEquals(expected, actual);
    }

    @Test
    void convert_throws_IllegalArgumentException_if_the_list_misses_hosts() {
        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> originalGraph = getDefaultGraph();

        assertThrows(IllegalArgumentException.class, () -> {
            adapter.convert(originalGraph, new ArrayList<>(hostsArrayList.subList(0, 1)));
        });
    }

    @Test
    void revert() {
        MutualContactGraph graph = new MutualContactGraph(new String[]{"0,1,0.5", "1,2,0.3", "0,2,1.5"});

        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> expected = getDefaultGraph();
        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> actual = adapter.revert(graph, hostsArrayList, emptySet());

        assertGraphsEquals(expected, actual);
    }

    @Test
    void revert_skips_hostsToExclude() {
        MutualContactGraph graph = new MutualContactGraph(new String[]{"0,1,0.5", "1,2,0.3", "0,2,1.5"});

        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> actual = adapter.revert(graph, hostsArrayList, buildSet(defaultGraphHost1));

        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> expected = new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);
        expected.addVertex(defaultGraphHost2);
        expected.addVertex(defaultGraphHost3);
        addEdge(expected, defaultGraphHost2, defaultGraphHost3, 0.3);

        assertGraphsEquals(expected, actual);
    }

    @Test
    void convert_and_revert_work_correctly() {
        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> originalGraph = getDefaultGraph();

        MutualContactGraph converted = adapter.convert(originalGraph, hostsArrayList);
        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> reverted = adapter.revert(converted, hostsArrayList);

        assertGraphsEquals(originalGraph, reverted);
    }


    private void assertGraphsEquals(DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph1,
                                    DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph2) {

        assertEquals(graph1.vertexSet(), graph2.vertexSet());

        Map<Couple<Host>, Double> graph1EdgeMap = new HashMap<>();
        Map<Couple<Host>, Double> graph2EdgeMap = new HashMap<>();

        graph1.edgeSet().forEach(edge -> {
            Host source = graph1.getEdgeSource(edge);
            Host target = graph1.getEdgeTarget(edge);
            double weight = graph1.getEdgeWeight(edge);

            graph1EdgeMap.put(new Couple<>(source, target), weight);
        });

        graph2.edgeSet().forEach(edge -> {
            Host source = graph1.getEdgeSource(edge);
            Host target = graph1.getEdgeTarget(edge);
            double weight = graph1.getEdgeWeight(edge);

            graph2EdgeMap.put(new Couple<>(source, target), weight);
        });

        assertEquals(graph1EdgeMap, graph2EdgeMap);
    }

    private DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> getDefaultGraph() {
        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph = new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);

        graph.addVertex(defaultGraphHost1);
        graph.addVertex(defaultGraphHost2);
        graph.addVertex(defaultGraphHost3);

        addEdge(graph, defaultGraphHost1, defaultGraphHost2, 0.5);
        addEdge(graph, defaultGraphHost2, defaultGraphHost3, 0.3);
        addEdge(graph, defaultGraphHost1, defaultGraphHost3, 1.5);

        return graph;
    }

    private void addEdge(DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph, Host host1, Host host2, double weight) {
        DefaultWeightedEdge edge3 = graph.addEdge(host1, host2);
        graph.setEdgeWeight(edge3, weight);
    }
}