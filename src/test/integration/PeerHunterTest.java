package test.integration;

import main.Params;
import main.PeerHunter;
import main.common.NetworkFlow;
import org.junit.jupiter.api.Test;
import test.dataset.BotnetDataset;
import test.dataset.ISCXTrainingSet;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;


class PeerHunterTest {

    @Test
    void getP2PBotsIps_fake_test() throws IOException {
        BotnetDataset dataset = new ISCXTrainingSet();
        Collection<NetworkFlow> networkFlows = dataset.getNetworkFlows();

        Params params = new Params(5, 0.1f, 0.1f, 0.1f);

        Set<String> p2pBotsIps = new PeerHunter(params).computeDetectionData(networkFlows).getBotIps();

        assertFalse(p2pBotsIps.isEmpty());
    }

}