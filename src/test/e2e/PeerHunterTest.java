package test.e2e;

import main.Params;
import main.PeerHunter;
import main.common.NetworkFlow;
import main.model.DetectionData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.dataset.BotnetDataset;
import test.dataset.PeerRushDataset;
import test.util.Partition;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PeerHunterTest {

    private static BotnetDataset dataset;
    private static Set<String> maliciousIps;
    private static List<NetworkFlow> networkFlows;

    private static Params params;

    @BeforeAll
    static void beforeAll() throws IOException {
        dataset = new PeerRushDataset();
        networkFlows = dataset.getNetworkFlows();
        params = new Params();
        params.useLighterAvgMCR = true;
    }

    @BeforeEach
    void beforeEach() throws IOException {
        maliciousIps = dataset.getMaliciousHostsIps();
    }

    @Test
    void peerrushDatasetTest() {
        Set<String> p2pBotsIps = new PeerHunter(params).computeDetectionData(networkFlows).getBotIps();
        assertEquals(maliciousIps.size(), p2pBotsIps.size(), 1);

        maliciousIps.removeAll(p2pBotsIps);
        assertEquals(1, maliciousIps.size());
        System.err.println("malicious ips not found: " + maliciousIps);
    }

    @Test
    void peerrushDataset_in_10_chunks_test() {
        int chunkSize = 300000;
        Partition<NetworkFlow> partition = Partition.ofSize(networkFlows, chunkSize);

        DetectionData detectionData = DetectionData.getEmptyData();

        for (List<NetworkFlow> networkFlows : partition) {
            detectionData = new PeerHunter(params, detectionData).computeDetectionData(networkFlows);
        }

        Set<String> p2pBotsIps = detectionData.getBotIps();
        assertEquals(maliciousIps.size(), p2pBotsIps.size(), 1);

        maliciousIps.removeAll(p2pBotsIps);
        assertEquals(1, maliciousIps.size());
        System.err.println("malicious ips not found: " + maliciousIps);
    }

    @Test
    void peerrushDataset_memory_usage() {
        Runtime runtime = Runtime.getRuntime();

        DetectionData detectionData = new PeerHunter(params).computeDetectionData(networkFlows);

        NumberFormat format = NumberFormat.getInstance();

        StringBuilder sb = new StringBuilder();
        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();

        sb.append("free memory: ").append(format.format(freeMemory / 1024)).append("\n");
        sb.append("allocated memory: ").append(format.format(allocatedMemory / 1024)).append("\n");
        sb.append("max memory: ").append(format.format(maxMemory / 1024)).append("\n");
        sb.append("total free memory: ").append(format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024)).append("\n");

        System.out.println(sb.toString());
    }
}
