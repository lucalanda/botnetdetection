package test.unit;

import main.Params;
import main.common.CommunicationCluster;
import main.common.Host;
import main.detection.P2PBotsDetector;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Set;

import static main.common.IPV4Utils.convertIP;
import static main.common.IPV4Utils.convertedIpv4Set;
import static main.common.ProtocolUtils.convertProtocol;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static test.fixtures.FixturesGenerator.aCommunicationClusterSet;
import static test.fixtures.FixturesGenerator.anHost;
import static test.util.Util.buildSet;

class P2PBotsDetectorTest {

    private P2PBotsDetector extractor;
    private DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph;
    private Set<Host> botnetCommunity;

    private Params lightParams = new Params();

    @BeforeEach
    void setUp() {
        graph = new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);

        lightParams.averageDestinationDiversityRatioThreshold = 0.1f;
        lightParams.averageMutualContactRatioThreshold = 0.1f;
        lightParams.useLighterAvgMCR = true;

        extractor = new P2PBotsDetector(graph, lightParams);

        CommunicationCluster cluster1 = new CommunicationCluster(
                convertedIpv4Set("10.11.12.13", "11.12.13.14", "11.12.14.15").toArray(), convertProtocol("tcp"), 10, 10, true);

        CommunicationCluster cluster2 = new CommunicationCluster(
                convertedIpv4Set("11.12.13.14", "12.13.14.15", "90.90.90.90", "12.13.0.0", "12.13.1.1").toArray(), convertProtocol("tcp"), 10, 10, true);

        CommunicationCluster cluster3 = new CommunicationCluster(
                convertedIpv4Set("10.11.12.13", "90.90.90.90", "0.0.0.0", "90.90.1.2").toArray(), convertProtocol("tcp"), 10, 10, true);

        botnetCommunity = buildSet(
                new Host(true, convertIP("10.10.10.10"), buildSet(cluster1)),
                new Host(true, convertIP("20.20.20.20"), buildSet(cluster2)),
                new Host(true, convertIP("30.30.30.30"), buildSet(cluster3))
        );
    }

    @Test
    void getAverageDestinationDiversityRatio() {
        double actual = extractor.getAverageDestinationDiversityRatio(botnetCommunity);

        assertEquals(0.671, actual, 0.001);
    }

    @Test
    void getAverageMutualContactsRatio() {
        float actual = extractor.getAverageMutualContactsRatio(botnetCommunity);

        assertEquals(0.144, actual, 0.001);
    }

    @Test
    void getAverageMutualContactsRatio_with_singleton_community() {
        float actual = extractor.getAverageMutualContactsRatio(buildSet(anHost()));

        assertEquals(0, actual);
    }

    @Test
    void isBotnet() {
        assert (extractor.isBotnet(botnetCommunity));

        Set<Host> botnetWithLowAvgDDR = buildSet(
                new Host(true, convertIP("10.10.10.10"), aCommunicationClusterSet("10.11.12.13", "11.12.13.14", "12.13.14.15")),
                new Host(true, convertIP("20.20.20.20"), aCommunicationClusterSet("11.12.13.14", "12.13.14.15", "90.90.90.90")),
                new Host(true, convertIP("30.30.30.30"), aCommunicationClusterSet("10.11.12.13", "90.90.90.90", "0.0.0.0"))
        );
        Set<Host> botnetWithLowAVGMCR = buildSet(anHost(), anHost(), anHost());

        assertFalse(extractor.isBotnet(botnetWithLowAvgDDR));
        assertFalse(extractor.isBotnet(botnetWithLowAVGMCR));
    }

    @Test
    void extractBots() {
        botnetCommunity.addAll(buildSet(anHost(), anHost()));

        botnetCommunity.forEach(graph::addVertex);

        ArrayList<Host> hosts = new ArrayList<>(botnetCommunity);
        graph.addEdge(hosts.get(0), hosts.get(1));
        graph.addEdge(hosts.get(1), hosts.get(2));
        graph.addEdge(hosts.get(0), hosts.get(2));

        graph.addEdge(hosts.get(0), hosts.get(3));
        graph.addEdge(hosts.get(1), hosts.get(3));
        graph.addEdge(hosts.get(1), hosts.get(4));

        Set<Host> expectedBotsSet = buildSet(hosts.get(0), hosts.get(1), hosts.get(2), hosts.get(3));
        Set<Host> actualBotsSet = extractor.extractBots(graph, botnetCommunity);

        assertEquals(expectedBotsSet, actualBotsSet);
    }

    @Test
    void getBots() {
        CommunicationCluster cluster4 = new CommunicationCluster(
                convertedIpv4Set("10.11.12.13", "90.90.90.90", "0.0.0.0").toArray(), convertProtocol("tcp"), 10, 10, true);
        CommunicationCluster cluster5 = new CommunicationCluster(
                convertedIpv4Set("10.11.12.13", "90.90.90.90", "0.0.0.0").toArray(), convertProtocol("tcp"), 10, 10, true);

        botnetCommunity.addAll(buildSet(
                new Host(true, convertIP("40.40.40.40"), buildSet(cluster4)),
                new Host(true, convertIP("50.50.50.50"), buildSet(cluster5))
        ));
        botnetCommunity.forEach(graph::addVertex);

        ArrayList<Host> hosts = new ArrayList<>(botnetCommunity);
        graph.addEdge(hosts.get(0), hosts.get(1));
        graph.addEdge(hosts.get(1), hosts.get(2));
        graph.addEdge(hosts.get(0), hosts.get(2));

        graph.addEdge(hosts.get(0), hosts.get(3));
        graph.addEdge(hosts.get(1), hosts.get(4));

        Set<Host> expectedBotsSet = buildSet(hosts.get(0), hosts.get(1), hosts.get(2));
        Set<Host> actualBotsSet = new P2PBotsDetector(graph, lightParams).getBots();

        assertEquals(expectedBotsSet, actualBotsSet);
    }
}
