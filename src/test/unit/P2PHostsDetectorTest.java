package test.unit;

import main.HostsExtractor;
import main.common.CommunicationCluster;
import main.common.Host;
import main.common.NetworkFlow;
import main.detection.P2PHostsDetector;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toSet;
import static junit.framework.TestCase.assertTrue;
import static main.common.IPV4Utils.convertIP;
import static main.common.IPV4Utils.convertedIpv4Set;
import static main.common.ProtocolUtils.convertProtocol;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.util.Assertions.raises;
import static test.util.Util.buildArray;
import static test.util.Util.buildSet;

class P2PHostsDetectorTest {

    private static final int DEFAULT_DESTINATION_DIVERSITY_RATIO = 1;
    private static final P2PHostsDetector detector = new P2PHostsDetector(DEFAULT_DESTINATION_DIVERSITY_RATIO);
    private static final HostsExtractor hostsExtractor = new HostsExtractor();

    @Test
    void getP2PHostsIps_with_no_hosts() {
        Collection<Host> hosts = emptyList();

        assertFalse(raises(() -> detector.flagP2PHosts(hosts)));
    }

    @Test
    void flagP2PHosts() {
        List<NetworkFlow> networkFlows = Arrays.asList(
                new NetworkFlow("10.10.1.2", "10.10.1.3", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("10.10.1.2", "10.11.1.4", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("10.10.1.2", "10.12.2.1", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("10.10.1.2", "10.13.2.1", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("10.10.1.2", "10.14.5.5", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("20.20.0.0", "10.10.1.3", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("20.20.0.0", "10.10.1.4", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("20.20.0.0", "10.10.0.0", convertProtocol("tcp"), 10, 10)
        );
        List<Host> hosts = hostsExtractor.extractUpdatedHostsList(networkFlows);

        detector.flagP2PHosts(hosts);
        Set<Host> expected = buildSet(hosts.get(0));
        Set<Host> actual = hosts.stream().filter(Host::isP2P).collect(toSet());

        assertEquals(expected, actual);
    }

    @Test
    void flagP2PHostsCommunicationClusters() {
        CommunicationCluster cluster1 = new CommunicationCluster(
                convertedIpv4Set("10.10.1.2", "10.11.3.4").toArray(), convertProtocol("tcp"), 10, 10, false
        );
        CommunicationCluster cluster2 = new CommunicationCluster(
                convertedIpv4Set("1.2.0.0").toArray(), convertProtocol("udp"), 10, 10, false
        );
        CommunicationCluster cluster3 = new CommunicationCluster(
                convertedIpv4Set("1.2.0.0", "3.4.5.6", "5.6.7.8").toArray(), convertProtocol("tcp"), 10, 10, false
        );
        CommunicationCluster cluster4 = new CommunicationCluster(
                convertedIpv4Set("1.2.0.0").toArray(), convertProtocol("udp"), 10, 10, false
        );

        Host host1 = new Host(false, convertIP("1.2.3.4"), buildArray(buildSet(cluster1, cluster2)));
        Host host2 = new Host(false, convertIP("5.6.7.8"), buildArray(buildSet(cluster3, cluster4)));

        detector.flagP2PHosts(buildSet(host1, host2));

        assertTrue(host1.isP2P());
        assertTrue(host2.isP2P());

        assertTrue(cluster1.isP2P());
        assertFalse(cluster2.isP2P());
        assertTrue(cluster3.isP2P());
        assertFalse(cluster4.isP2P());
    }

}
