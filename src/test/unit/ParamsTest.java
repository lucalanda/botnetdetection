package test.unit;

import main.Params;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParamsTest {
    @Test
    void getDefault_values() {
        Params defaultParams = Params.getDefault();

        assertEquals(50, defaultParams.destinationDiversityThreshold);
        assertEquals(0.03125, defaultParams.mutualContactRatioThreshold);
        assertEquals(0.0625, defaultParams.averageDestinationDiversityRatioThreshold);
        assertEquals(0.25, defaultParams.averageMutualContactRatioThreshold, 0.1f);
    }
}
