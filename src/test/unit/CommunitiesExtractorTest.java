package test.unit;

import cwts.networkanalysis.Clustering;
import cwts.networkanalysis.Network;
import gnu.trove.map.hash.TObjectIntHashMap;
import main.common.Host;
import main.detection.CommunitiesExtractor;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.fixtures.FixturesGenerator.anHost;
import static test.util.Assertions.assertArrayEqualsInAnyOrder;
import static test.util.Util.*;

class CommunitiesExtractorTest {

    private final Host[] hosts = buildArray(
            anHost(), anHost(), anHost(), anHost()
    );
    private final CommunitiesExtractor extractor = new CommunitiesExtractor(getDefaultGraph());

    @Test
    void toArray() {
        Host[] hosts = buildArray(anHost(), anHost());

        Collection<Host> hostCollection = buildSet(hosts);
        Host[] actual = extractor.toArray(hostCollection);
        assertArrayEqualsInAnyOrder(hosts, actual);
    }

    @Test
    void getHostIdsMap() {
        Host[] hosts = buildArray(anHost(), anHost());

        TObjectIntHashMap<Host> expected = new TObjectIntHashMap<>();
        expected.put(hosts[0], 0);
        expected.put(hosts[1], 1);

        TObjectIntHashMap<Host> actual = extractor.getHostsIdsMap(hosts);
        assertEquals(expected, actual);
    }

    @Test
    void buildNetwork() {
        TObjectIntHashMap<Host> hostIdsMap = extractor.getHostsIdsMap(hosts);

        Network network = extractor.buildNetwork(hostIdsMap);

        assertEquals(4, network.getNNodes());
        assertEquals(3, network.getNEdges());

        assertArrayEquals(doubleArray(50, 30), network.getEdgeWeights(0));
        assertArrayEquals(doubleArray(50, 20), network.getEdgeWeights(1));
        assertArrayEquals(doubleArray(30, 20), network.getEdgeWeights(2));

        assertArrayEquals(intArray(1, 2), network.getNeighbors(0));
        assertArrayEquals(intArray(0, 2), network.getNeighbors(1));
        assertArrayEquals(intArray(0, 1), network.getNeighbors(2));
    }

    @Test
    void convertClusteringToCommunitiesCollection() {
        Clustering clustering = new Clustering(intArray(0, 0, 0, 1));

        Collection<Set<Host>> actualCommunities = extractor.convertClusteringToCommunitiesCollection(clustering, hosts);

        Collection<Set<Host>> expectedCommunities = Arrays.asList(
                buildSet(hosts[0], hosts[1], hosts[2]),
                buildSet(hosts[3])
        );

        assertEquals(expectedCommunities, actualCommunities);
    }

    @Test
    void getCommunities() {
        Collection<Set<Host>> expectedCommunities = Arrays.asList(
                buildSet(hosts[0], hosts[1], hosts[2]),
                buildSet(hosts[3])
        );

        assertEquals(expectedCommunities, extractor.getCommunities());
    }

    @Test
    void getCommunities_with_empty_graph_given() {
        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph = new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);

        Collection<Set<Host>> communities = new CommunitiesExtractor(graph).getCommunities();

        assertEquals(0, communities.size());
    }

    private DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> getDefaultGraph() {
        DefaultUndirectedWeightedGraph<Host, DefaultWeightedEdge> graph = new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);

        Arrays.stream(hosts).forEach(graph::addVertex);

        DefaultWeightedEdge edge1 = graph.addEdge(hosts[0], hosts[1]);
        graph.setEdgeWeight(edge1, 0.5);

        DefaultWeightedEdge edge2 = graph.addEdge(hosts[1], hosts[2]);
        graph.setEdgeWeight(edge2, 0.2);

        DefaultWeightedEdge edge3 = graph.addEdge(hosts[0], hosts[2]);
        graph.setEdgeWeight(edge3, 0.3);

        return graph;
    }

}
