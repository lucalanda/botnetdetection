package test.unit;

import main.HostsExtractor;
import main.common.CommunicationCluster;
import main.common.Host;
import main.common.NetworkFlow;
import main.detection.MutualContactGraphExtractor;
import org.jgrapht.Graph;
import org.jgrapht.GraphType;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.util.Collections.emptyList;
import static junit.framework.TestCase.assertTrue;
import static main.common.IPV4Utils.convertIP;
import static main.common.IPV4Utils.convertedIpv4Set;
import static main.common.ProtocolUtils.convertProtocol;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static test.util.Util.buildArray;
import static test.util.Util.buildSet;

class MutualContactGraphExtractorTest {

    private static final float DEFAULT_MUTUAL_CONTACT_RATIO_THRESHOLD = 0.4f;
    private static MutualContactGraphExtractor extractor;

    @BeforeEach
    void setUp() {
        extractor = new MutualContactGraphExtractor(DEFAULT_MUTUAL_CONTACT_RATIO_THRESHOLD);
    }

    @Test
    void getGraph_returns_undirected_graph() {
        Graph<Host, DefaultWeightedEdge> graph = extractor.getGraph(emptyList());
        GraphType type = graph.getType();
        assertFalse(type.isDirected());
        assertTrue(type.isWeighted());
    }

    @Test
    void getGraph_adds_all_nodes_to_graph() {
        Collection<NetworkFlow> networkFlows = Arrays.asList(
                new NetworkFlow("10.10.0.0", "10.20.30.40", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("20.20.0.0", "20.10.00.00", convertProtocol("udp"), 10, 10),
                new NetworkFlow("30.30.0.0", "30.20.10.00", convertProtocol("udp"), 10, 10)
        );

        Collection<Host> hosts = getHostsFor(networkFlows);

        Graph<Host, DefaultWeightedEdge> graph = extractor.getGraph(hosts);

        Set<Host> expected = new HashSet<>(hosts);
        Set<Host> actual = graph.vertexSet();

        assertEquals(expected, actual);
    }

    @Test
    void getGraph_does_not_add_edges_with_no_patterns_set_intersection() {
        Collection<NetworkFlow> networkFlows = Arrays.asList(
                new NetworkFlow("10.10.0.0", "10.20.30.40", convertProtocol("tcp"), 10, 10),
                new NetworkFlow("20.20.0.0", "10.20.30.40", convertProtocol("udp"), 10, 10)
        );

        Collection<Host> hosts = getHostsFor(networkFlows);

        Graph<Host, DefaultWeightedEdge> graph = extractor.getGraph(hosts);
        assertEquals(0, graph.edgeSet().size());
    }

    @Test
    void getGraph_does_not_add_edge_with_patterns_set_intersection_but_insufficient_mcr() {
        CommunicationCluster cluster1 = new CommunicationCluster(
                convertedIpv4Set("10.10.1.2", "10.11.3.4").toArray(), convertProtocol("tcp"), 10, 10, true
        );
        CommunicationCluster cluster2 = new CommunicationCluster(
                convertedIpv4Set("1.2.0.0", "3.4.5.6", "5.6.7.8").toArray(), convertProtocol("tcp"), 10, 10, true
        );

        Host host1 = new Host(true, convertIP("10.10.0.0"), buildArray(buildSet(cluster1)));
        Host host2 = new Host(true, convertIP("20.20.0.0"), buildArray(buildSet(cluster2)));

        Collection<Host> hosts = buildSet(host1, host2);
        Graph<Host, DefaultWeightedEdge> graph = extractor.getGraph(hosts);
        assertEquals(0, graph.edgeSet().size());
    }

    @Test
    void getGraph_adds_vertex_when_cluster_sets_intersect_and_mcr_is_enough() {
        CommunicationCluster cluster1 = new CommunicationCluster(
                convertedIpv4Set("10.10.1.2", "1.2.0.0", "3.4.5.6").toArray(), convertProtocol("tcp"), 10, 10, true
        );
        CommunicationCluster cluster2 = new CommunicationCluster(
                convertedIpv4Set("1.2.0.0", "3.4.5.6", "5.6.7.8").toArray(), convertProtocol("tcp"), 10, 10, true
        );

        Host host1 = new Host(true, convertIP("10.10.0.0"), buildArray(buildSet(cluster1)));
        Host host2 = new Host(true, convertIP("20.20.0.0"), buildArray(buildSet(cluster2)));

        Collection<Host> hosts = buildSet(host1, host2);

        Graph<Host, DefaultWeightedEdge> graph = extractor.getGraph(hosts);

        assertEquals(1, graph.edgeSet().size());

        DefaultWeightedEdge edge = (DefaultWeightedEdge) graph.edgeSet().toArray()[0];
        float expectedEdgeWeight = 0.5f;
        assertEquals(expectedEdgeWeight, graph.getEdgeWeight(edge));

        // TODO make better
        Set<Integer> expectedHostsIps = buildSet(convertIP("10.10.0.0"), convertIP("20.20.0.0"));
        Set<Integer> actualHostsIps = buildSet(graph.getEdgeSource(edge).getIp(), graph.getEdgeTarget(edge).getIp());
        assertEquals(expectedHostsIps, actualHostsIps);
    }

    @Test
    void getGraph_returns_empty_graph_with_no_network_flows_given() {
        Graph<Host, DefaultWeightedEdge> graph = extractor.getGraph(emptyList());
        assertEquals(0, graph.vertexSet().size());
        assertEquals(0, graph.edgeSet().size());
    }

    private static Collection<Host> getHostsFor(Collection<NetworkFlow> networkFlows) {
        return new HostsExtractor().extractUpdatedHostsList(networkFlows);
    }
}
