package test.unit.examples;

import org.jgrapht.alg.clique.BronKerboschCliqueFinder;
import org.jgrapht.graph.AsSubgraph;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.platform.commons.util.CollectionUtils.getOnlyElement;
import static test.util.Util.buildSet;

class jGraphUsageTest {

    DefaultUndirectedWeightedGraph<String, DefaultWeightedEdge> graph;

    @BeforeEach
    void setUp() {
        graph = new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);
    }


    @Test
    void DefaultUndirectedWeightedGraphExample() {
        graph.addVertex("1");
        graph.addVertex("2");

        DefaultWeightedEdge edge = graph.addEdge("1", "2");
        graph.setEdgeWeight(edge, 5);

        assertNull(graph.addEdge("2", "1"));

        graph.addEdge("1", "2");
        assertEquals(1, graph.getAllEdges("1", "2").size());
        assertEquals(1, graph.getAllEdges("2", "1").size());

        assertEquals(5.0, graph.getEdgeWeight(edge));
    }

    @Test
    void subgraphExample() {
        Set<String> vertexes = buildSet("0", "1", "2", "3");
        vertexes.forEach(graph::addVertex);

        DefaultWeightedEdge edge1 = graph.addEdge("0", "1");
        DefaultWeightedEdge edge2 = graph.addEdge("1", "2");
        DefaultWeightedEdge edge3 = graph.addEdge("0", "2");

        graph.setEdgeWeight(edge1, 3);
        graph.setEdgeWeight(edge2, 4);
        graph.setEdgeWeight(edge3, 5);

        AsSubgraph<String, DefaultWeightedEdge> subgraph = new AsSubgraph<>(graph, buildSet("0", "1"));

        assertEquals(buildSet("0", "1"), subgraph.vertexSet());
        assertEquals(1, subgraph.edgeSet().size());

        DefaultWeightedEdge subgraphEdge = getOnlyElement(subgraph.edgeSet());
        assertEquals(3.0, subgraph.getEdgeWeight(subgraphEdge));
    }

    @Test
    void findMaximumCliquesExample() {
        Set<String> vertexes = buildSet("0", "1", "2", "3", "4");
        vertexes.forEach(graph::addVertex);

        graph.addEdge("0", "1");
        graph.addEdge("1", "2");
        graph.addEdge("0", "2");
        graph.addEdge("0", "3");

        BronKerboschCliqueFinder cliqueFinder = new BronKerboschCliqueFinder(graph);
        Set<Set<String>> expectedCliqueSet = buildSet(
                buildSet("0", "1", "2"),
                buildSet("0", "3"),
                buildSet("4")
        );

        Set<Set<String>> actualCliqueSet = new HashSet<>();
        cliqueFinder.forEach(set -> actualCliqueSet.add((Set<String>) set));

        assertEquals(expectedCliqueSet, actualCliqueSet);
    }
}
