package test.unit.examples;

import gnu.trove.TIntCollection;
import gnu.trove.set.hash.TIntHashSet;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TroveUsageTest {

    @Test
    void toArray_conversion_works() {
        TIntCollection collection = new TIntHashSet();

        collection.add(1);
        collection.add(50);
        collection.add(100000);

        int[] expected = new int[]{100000, 50, 1};
        assertArrayEquals(expected, collection.toArray());
    }
}
