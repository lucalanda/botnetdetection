package test.unit;

import main.common.NetworkFlow;
import org.junit.jupiter.api.Test;

import static main.common.ProtocolUtils.convertProtocol;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static test.util.Assertions.raises;

class NetworkFlowTest {
    private String uncorrectIp = "uncorrect ip";
    private String ipv4 = "10.10.10.10";
    private String ipv6 = "a1:a2:c:b1:b2";

    @Test
    void correctFormat() {
        assertFalse(raises(() -> new NetworkFlow(ipv4, ipv4, convertProtocol("tcp"), 1000, 1000)));
    }

    @Test
    void ipsMustBeInCorrectFormat() {
        assert (raises(() -> new NetworkFlow(ipv4, uncorrectIp, convertProtocol("tcp"), 1000, 1000)));
        assert (raises(() -> new NetworkFlow(uncorrectIp, ipv4, convertProtocol("tcp"), 1000, 1000)));
        assert (raises(() -> new NetworkFlow(ipv6, ipv4, convertProtocol("tcp"), 1000, 1000)));
        assert (raises(() -> new NetworkFlow(uncorrectIp, ipv6, convertProtocol("tcp"), 1000, 1000)));
    }

}