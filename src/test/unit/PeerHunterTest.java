package test.unit;

import gnu.trove.list.array.TIntArrayList;
import main.PeerHunter;
import main.common.Host;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static junit.framework.TestCase.assertSame;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.fixtures.FixturesGenerator.aCommunicationClusterSet;
import static test.fixtures.FixturesGenerator.anHost;
import static test.util.Assertions.raises;

public class PeerHunterTest {

    private static PeerHunter peerHunter = new PeerHunter();

    @Test
    void getNewOrUpdatedHosts_works_with_empty_sets() {
        assertFalse(raises(() -> {
            peerHunter.getNewOrUpdatedHosts(new TIntArrayList(), new ArrayList<>());
        }));
    }


    @Test
    void getNewOrUpdatedHosts_works_with_no_previous_hosts() {
        List<Host> hosts = asList(anHost(), anHost(), anHost());

        assertFalse(raises(() -> {
            peerHunter.getNewOrUpdatedHosts(new TIntArrayList(), new ArrayList<>(hosts));
        }));
    }

    @Test
    void getNewOrUpdatedHosts_works_correctly() {
        List<Host> currentHosts = asList(anHost(), anHost(), anHost());

        TIntArrayList currentHashCodes = new TIntArrayList(currentHosts.size());
        currentHosts.forEach(host -> currentHashCodes.add(host.realHashCode()));

        currentHosts.get(0).update(aCommunicationClusterSet());

        ArrayList<Host> newHostList = new ArrayList<>(currentHosts);
        Host newHost = anHost();
        newHostList.add(newHost);

        List<Host> newOrUpdatedHosts = new PeerHunter().getNewOrUpdatedHosts(currentHashCodes, newHostList);

        assertEquals(2, newOrUpdatedHosts.size());
        assertSame(currentHosts.get(0), newOrUpdatedHosts.get(0));
        assertSame(newHost, newOrUpdatedHosts.get(1));
    }

}
