package test.fixtures;

import gnu.trove.set.hash.TIntHashSet;
import main.common.CommunicationCluster;
import main.common.Host;

import java.util.Arrays;
import java.util.Random;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static main.common.IPV4Utils.convertIP;
import static test.util.Util.buildArray;
import static test.util.Util.buildSet;

public class FixturesGenerator {
    private static final Random random = new Random(42);
    private static final byte[] supportedProtocols = new byte[]{(byte) 0, (byte) 1, (byte) 2};

    public static Host anHost() {
        return new Host(false, anIpv4(), buildArray(aCommunicationClusterSet()));
    }

    public static Host anHostWithContacts(String... ips) {
        return new Host(false, anIpv4(), buildArray(aCommunicationClusterSet(ips)));
    }

    public static CommunicationCluster aCommunicationCluster() {
        return new CommunicationCluster(anIpv4Set(new String[0]), aProtocol(), aBPPValue(), aBPPValue());
    }

    public static CommunicationCluster aCommunicationCluster(String... ips) {
        return new CommunicationCluster(anIpv4Set(ips), aProtocol(), aBPPValue(), aBPPValue());
    }

    public static Set<CommunicationCluster> aCommunicationClusterSet() {
        return buildSet(aCommunicationCluster());
    }

    public static Set<CommunicationCluster> aCommunicationClusterSet(String... ips) {
        return Arrays.stream(ips).map(FixturesGenerator::aCommunicationCluster).collect(toSet());
    }

    public static byte aProtocol() {
        return supportedProtocols[random.nextInt(supportedProtocols.length)];
    }

    public static String anIpv4String() {
        return random.nextInt(256) + "." + random.nextInt(256) + "." + random.nextInt(256) + "." + random.nextInt(256);
    }

    public static int anIpv4() {
        return convertIP(anIpv4String());
    }

    public static int[] anIpv4Set(String[] ips) {
        TIntHashSet result = new TIntHashSet();

        for(String ip : ips) result.add(convertIP(ip));

        if(ips.length == 0) result.add(anIpv4());

        return result.toArray();
    }

    public static int aBPPValue() {
        return random.nextInt(1000);
    }
}
